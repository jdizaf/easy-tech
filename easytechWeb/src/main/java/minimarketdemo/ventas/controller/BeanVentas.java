package minimarketdemo.ventas.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import minimarketdemo.controller.JSFUtil;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import java.math.BigDecimal;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.inject.Inject;
import javax.inject.Named;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.controller.seguridades.BeanSegLogin;
import minimarketdemo.model.core.entities.TblCategoria;
import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblDetalleFactura;
import minimarketdemo.model.core.entities.TblDetallesCompra;
import minimarketdemo.model.core.entities.TblFactPro;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.entities.TblProducto;
import minimarketdemo.model.core.entities.TblTDocumento;
import minimarketdemo.model.core.entities.TblTasaNominal;
import minimarketdemo.model.core.entities.TblTipoPago;
import minimarketdemo.model.ventas.dtos.DTOcategorias;
import minimarketdemo.model.ventas.dtos.DTOclientes;
import minimarketdemo.model.ventas.dtos.DTOpersona;
import minimarketdemo.model.ventas.dtos.DTOproducto;
import minimarketdemo.model.ventas.managers.ManagerCliente;
import minimarketdemo.model.ventas.managers.ManagerVentas;

@Named
@SessionScoped
public class BeanVentas implements Serializable {

	@EJB
	private ManagerVentas mVentas;

	private DTOclientes dtoCliente;
	private DTOpersona dtoPersonas;
	private TblCliente cliente;
	private TblCliente persona;
	private TblFactPro cabecera;
	private DTOcategorias dtoCategorias;
	private DTOproducto dtoProducto;
	private TblCliente edicionCliente;
	private Date fechaActual;
	private TblTipoPago dtoTipoPago;
	// Variables extras
	private int numcolumns;
	private int numFactPro;
	private BigDecimal precioseleccionado;
	private int idProducto;
	private String cedula;
	private int idTipopago;
	private String descripcinonTipopago;
	private int idTipodoc;
	private int cantidadseleccionada;
	@Inject
	private BeanSegLogin beanSegLogin;

	// variables extras
	private String buscarCliente;
	private int idCategoriaPrincipal;
	private int idSubCategoriaPrincipal;
	private String nombreCategoria;
	private int tiempoMeses;
	// seccion de venta
	// KF
	private TblCliente mostarCliente;
	private TblTipoPago mostarTipoPago;
	private TblProducto mostarPorducto;
	private TblTDocumento mostarDocumento;

	private List<BigDecimal> listaNuevoPrecio;
	private List<TblDetalleFactura> listaDetalleFacturas;
	private TblDetalleFactura detalleFactura;
	private int idAuxListaProducto;
	private BigDecimal subtotalListaDetalle;
	private BigDecimal totalListaDetalle;
	private BigDecimal ivaListaDetalle;
	private List<TblFactPro> listaFacturas;
	private List<TblFactPro> listaProformas;

	// Variables de Cuotas
	private List<TblTasaNominal> listaTasaNominal;
	private BigDecimal porcentajeSeleccionado;

	//// Listas
	private List<TblCategoria> listaCategorias;
	private List<TblCategoria> listaSubCategorias;
	private List<TblCategoria> listaSub_Categorias;
	private List<TblProducto> listaProductos;
	private List<TblCliente> listaClientes;
	private List<TblTipoPago> listapagos;
	private List<TblFactPro> listaNumfactura;
	private List<TblTDocumento> listaDocumentos;
	private List<BigDecimal> listaprecios;
	private List<BigDecimal> listapreciosAux;
	private List<TblDetalleFactura> listaDetalleFactura;
	private BigDecimal subTotal;

	// Botonos
	private String modBotonGuardarVenta;
	private String modBotonCuotas;
	private String modBotonInf;

	// Tasa Nominal
	private TblTasaNominal NuevaTasa;
	/*
	 * private List<String> listanombreprecios= [precio general, precio
	 * preferencial, precio]
	 */

	/////////
	private List<SelectItem> listasSelectItemSubcategorias;

	private SelectItem[] example;

	private List<DTOpersona> lista;

	@PostConstruct
	public void inicializar() {
		dtoCliente = new DTOclientes();
		dtoPersonas = new DTOpersona();
		dtoCategorias = new DTOcategorias();

		listaCategorias = mVentas.findCategoriaPrincipal();
		listaClientes = mVentas.findAllCliente();
		listapagos = mVentas.findAllTipoPago();
		listaDocumentos = mVentas.findAllTipoDocumento();
		listaprecios = new ArrayList<BigDecimal>();
		listaDetalleFactura = new ArrayList<TblDetalleFactura>();
		detalleFactura = new TblDetalleFactura();
		listaFacturas = mVentas.findAllFacturas();
		listaProformas = mVentas.findAllProformas();
		listaTasaNominal = mVentas.findAllTasaNominal();
		// listaSubCategorias = managerOrdenTrabajo.findSubCategorias(2);
		idCategoriaPrincipal = 0;
		idAuxListaProducto = 0;
		subTotal = BigDecimal.valueOf(0);
		fechaActual = new Date();
		numcolumns = 2;
		numFactPro = 0;
		NuevaTasa = new TblTasaNominal();
		modBotonCuotas = "Display:none";
		modBotonGuardarVenta = "Display:none";
		modBotonInf = "Display:none";
//		example = new SelectItem[listaSubCategorias.size()];
		listasSelectItemSubcategorias = new ArrayList<SelectItem>();

	}

	// Consultar Clientes
	public void actionFindAllCliente() throws Exception {
		listaClientes = mVentas.findAllCliente();

	}

	// Registar Tasa Nominal
	public void actionListenerInsertarTasa() {
		try {
			mVentas.insertarRegistro(NuevaTasa);
			listaTasaNominal = mVentas.findAllTasaNominal();
			NuevaTasa = new TblTasaNominal();
			JSFUtil.crearMensajeINFO("Tasa Nominal registrado");
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void actionnumFactura() {

		listaNumfactura = mVentas.getSiguenteNroFactura(idTipodoc);
		if (listaNumfactura.size() == 0) {
			numFactPro = 1;
		} else {
			for (int i = 0; i < listaNumfactura.size(); i++) {
				numFactPro = listaNumfactura.get(i).getIdFactura();

			}
		}
		System.out.println(numFactPro);

	}

	public void actionEjecutarTipoPago() {
		System.out.println("Id <>Tipo pago: " + idTipopago);
		if (idTipopago == 1) {
			modBotonCuotas = "Display:block";

			modBotonGuardarVenta = "Display:none";
		} else {
			modBotonGuardarVenta = "Display:block";
			modBotonCuotas = "Display:none";
		}
	}

	// Editar Cliente
	public void actionSeleccionarEdicionCliente(TblCliente clientes) {
		System.out.println("Agregados los clientes");
		edicionCliente = clientes;
	}

	public void actionEjecutarProductos() {
		listaProductos = mVentas.findProductos(nombreCategoria);
	}

	public void actionMostrarInformacionProductos() throws Exception {

		dtoProducto = mVentas.findProductoById(idProducto);
		listaprecios.clear();
		generarListaprecios();

		System.out.println(listaprecios);
	}

	public void generarListaprecios() throws Exception {
		listaprecios = mVentas.crearListaprecios(listaprecios, listaProductos, idProducto);
		listapreciosAux = listaprecios;
	}

	// Codigo version 2
	public void actionEjecutarCategorias() {

		listaCategorias = mVentas.findCategoriaPrincipal();
		listaSubCategorias = mVentas.findSubCategorias(idCategoriaPrincipal);
		listasSelectItemSubcategorias = new ArrayList<SelectItem>();
		int subcsubcategoria = 0;
		for (int j = 0; j < listaSubCategorias.size(); j++) {
			SelectItemGroup g1 = new SelectItemGroup(listaSubCategorias.get(j).getCategoria());
			subcsubcategoria = listaSubCategorias.get(j).getIdCategoria();

			listaSub_Categorias = mVentas.findSubCategorias(subcsubcategoria);

			if (listaSub_Categorias.size() == 0) {
				example = new SelectItem[1];
				for (int i = 0; i < 1; i++) {

					example[i] = new SelectItem(listaSubCategorias.get(j).getCategoria(),
							listaSubCategorias.get(j).getCategoria());

				}
				g1.setSelectItems(example);
			} else {
				example = new SelectItem[listaSub_Categorias.size()];
				for (int i = 0; i < listaSub_Categorias.size(); i++) {

					example[i] = new SelectItem(listaSub_Categorias.get(i).getCategoria(),
							listaSub_Categorias.get(i).getCategoria());

				}
				g1.setSelectItems(example);

			}

			listasSelectItemSubcategorias.add(g1);

		}

	}
	// codigo version 3

	public void actionRegistraCliente() {
		mVentas.registrarCliente(dtoPersonas, dtoCliente);
		listaClientes = mVentas.findAllCliente();

	}

	public void actionObtenerDatosCliente() {
		modBotonInf = "Display:block";
		dtoPersonas = mVentas.findClientByCedula(cedula);

	}

	// Insertar Venta
	public void actionListenerAdicionarProducto() {
		System.out.println(idTipopago);
		System.out.println(idTipodoc);
		mostarPorducto = mVentas.findProductoByPro(idProducto);
		if (cantidadseleccionada > mostarPorducto.getStock()) {
			JSFUtil.crearMensajeINFO("Stock Insuficiente.");
		} else {
			if (this.mostarPorducto != null) {

				if (listaDetalleFactura.isEmpty()) {
					idAuxListaProducto = 0;
					cabecera = new TblFactPro();
					TblDetalleFactura detalleAux = new TblDetalleFactura();

					detalleAux.setCantidad(cantidadseleccionada);
					detalleAux.setTblProducto(mostarPorducto);
					detalleAux.setPrecioTotal(precioseleccionado.multiply(BigDecimal.valueOf(cantidadseleccionada)));
					detalleAux.setPrecioselect(precioseleccionado);
					detalleAux.setTblFactPro(cabecera);
					idAuxListaProducto += 1;
					detalleAux.setIdDetalleFactura(idAuxListaProducto);
					listaDetalleFactura.add(detalleAux);
					subtotalListaDetalle = mVentas.subTotalDetalleFactura(listaDetalleFactura);
					ivaListaDetalle = subtotalListaDetalle.multiply(BigDecimal.valueOf(0.12));
					totalListaDetalle = subtotalListaDetalle.add(ivaListaDetalle);

				} else {
					if (mVentas.verificarExistencia(listaDetalleFactura, mostarPorducto)) {
						JSFUtil.crearMensajeINFO("Producto ya agregado");
					} else {
						cabecera = new TblFactPro();
						TblDetalleFactura detallaAux = new TblDetalleFactura();
						idAuxListaProducto = listaDetalleFactura.size();
						detallaAux.setCantidad(cantidadseleccionada);
						detallaAux.setTblProducto(mostarPorducto);
						detallaAux
								.setPrecioTotal(precioseleccionado.multiply(BigDecimal.valueOf(cantidadseleccionada)));
						detallaAux.setPrecioselect(precioseleccionado);
						detallaAux.setTblFactPro(cabecera);
						idAuxListaProducto += 1;
						detallaAux.setIdDetalleFactura(idAuxListaProducto);
						listaDetalleFactura.add(detallaAux);
						subtotalListaDetalle = mVentas.subTotalDetalleFactura(listaDetalleFactura);
						ivaListaDetalle = subtotalListaDetalle.multiply(BigDecimal.valueOf(0.12));
						totalListaDetalle = subtotalListaDetalle.add(ivaListaDetalle);

					}
				}
			} else {
				cantidadseleccionada = 0;
			}
		}
	}

	// Anular Factura
	public void anularFacturas(TblFactPro facturas) throws Exception {
		try {
			mVentas.anularFactura(facturas);
			JSFUtil.crearMensajeINFO("Factura Inactiva.");
			listaFacturas = mVentas.findAllFacturas();
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	// Activar Factura
	public void activarFacturas(TblFactPro facturas) throws Exception {
		try {
			mVentas.activarFactura(facturas);
			JSFUtil.crearMensajeINFO("Factura Activa.");
			listaFacturas = mVentas.findAllFacturas();
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	// Pasar de proforma a factura
	public String redirigirProforma(TblFactPro facturas) throws Exception {
		try {

			listaNumfactura = mVentas.getSiguenteNroFactura(1);
			if (listaNumfactura.size() == 0) {
				numFactPro = 1;
			} else {
				for (int i = 0; i < listaNumfactura.size(); i++) {
					numFactPro = listaNumfactura.get(i).getIdFactura();

				}
			}
			System.out.println("El numero de Factura siguiente es: " + numFactPro);
			mVentas.redirigirProforma(facturas, numFactPro);
			listaFacturas = mVentas.findAllFacturas();
			listaProformas = mVentas.findAllProformas();
			numFactPro = 0;
			return "menuFactura";
		} catch (Exception e) {
			// TODO: handle exception
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
			return "";
		}
	}

	public void limpiarIdListaAuxiliar() {
		for (int i = 0; i < listaDetalleFactura.size(); i++) {
			listaDetalleFactura.get(i).setIdDetalleFactura(null);
		}
	}

	public void actulizar() {
		listaClientes = mVentas.findAllCliente();
		listapagos = mVentas.findAllTipoPago();
		listaDocumentos = mVentas.findAllTipoDocumento();
		listaCategorias = mVentas.findCategoriaPrincipal();
	}

	// Guardar Venta
	public String actionListenerGuardarVenta() {
		TblCliente cliente = mVentas.ObtenerClientesByCedula(cedula);
		try {
			if (listaDetalleFactura.isEmpty()) {
				JSFUtil.crearMensajeWARN("Lista de detalle Vacia");
			} else {
				if (cabecera == null) {
					JSFUtil.crearMensajeINFO("La Cabecera esta Vacia");
				} else {
					if (idTipopago == 1 && idTipodoc == 1 || idTipopago == 2 && idTipodoc == 1) {
						System.out.println("Metodo 1");
						mVentas.guardarFactura(listaDetalleFactura, cliente, idTipopago, idTipodoc, idProducto,
								numFactPro, beanSegLogin.getIdSegUsuario());
						limpiarIdListaAuxiliar();
						cabecera = new TblFactPro();
						listaDetalleFactura.clear();
						subtotalListaDetalle = BigDecimal.valueOf(0);
						ivaListaDetalle = BigDecimal.valueOf(0);
						totalListaDetalle = BigDecimal.valueOf(0);
						numFactPro = 0;
						listaprecios.clear();
						// Limpiar listas
						listaClientes.clear();
						listaCategorias.clear();
						listapagos.clear();
						listaDocumentos.clear();
						// Recargar Factutas y Proformas
						listaFacturas = mVentas.findAllFacturas();
						listaProformas = mVentas.findAllProformas();
						dtoPersonas = new DTOpersona();
						modBotonInf = "Display:none";
						JSFUtil.crearMensajeINFO("Venta guardada exitosamente.");
						// Cargar listas
						actulizar();
						return "menuFactura";
					} else {
						System.out.println("Metodo 2");
						mVentas.guardarProforma(listaDetalleFactura, cliente, idTipopago, idTipodoc, idProducto,
								numFactPro, beanSegLogin.getIdSegUsuario());
						limpiarIdListaAuxiliar();
						cabecera = new TblFactPro();
						listaDetalleFactura.clear();
						numFactPro = 0;
						listaprecios.clear();
						// Limpiar listas
						listaClientes.clear();
						listaCategorias.clear();
						listapagos.clear();
						listaDocumentos.clear();

						/////////////
						subtotalListaDetalle = BigDecimal.valueOf(0);
						ivaListaDetalle = BigDecimal.valueOf(0);
						totalListaDetalle = BigDecimal.valueOf(0);
						listaFacturas = mVentas.findAllFacturas();
						listaProformas = mVentas.findAllProformas();
						dtoPersonas = new DTOpersona();
						modBotonInf = "Display:none";
						JSFUtil.crearMensajeINFO("Proforma guardada exitosamente.");
						// Cargar listas
						actulizar();
						return "menuProforma";
					}

				}

			}
			return "";
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}
	}

	// EliminarDetalle
	public void actionListenerEliminarDetalleListaTemporal(TblDetalleFactura detalleEliminar) {
		try {
			mVentas.borrarDetalleListaTemporal(listaDetalleFactura, detalleEliminar);
			// Datos del footer de la compras
			subtotalListaDetalle = mVentas.subTotalDetalleFactura(listaDetalleFactura);
			ivaListaDetalle = subtotalListaDetalle.multiply(BigDecimal.valueOf(0.12));
			totalListaDetalle = subtotalListaDetalle.add(ivaListaDetalle);
			idAuxListaProducto = 0;
		} catch (Exception e) {
			System.out.println(e);
		}

	}

	// metodo jasper reporte factura

	public String actionReporteProforma(TblFactPro facpro) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		int facturaSeleccionada = facpro.getIdFactura();
		parametros.put("idFactura", facturaSeleccionada);
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("ventas/reporte_proforma.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_proforma.pdf");
		response.setContentType("application/pdf");
		System.out.println("Reporte factura");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	// metodo jasper reporte factura

	public String actionReporteFactura(TblFactPro facpro) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		int facturaSeleccionada = facpro.getIdFactura();
		parametros.put("idFactura", facturaSeleccionada);
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("ventas/reporte_fact_pro_2.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_factura.pdf");
		response.setContentType("application/pdf");
		System.out.println("Reporte factura");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	public String actionListenerGenerarPagos() {
		try {

			TblCliente per = mVentas.ObtenerClientesByCedula(cedula);

			double porcentajeSelecionadoAux = porcentajeSeleccionado.doubleValue();
			double totalListaDetalleAux = totalListaDetalle.doubleValue();
			actionListenerGuardarVenta();
			mVentas.GenerarCuotasCreditoContrato(per, porcentajeSelecionadoAux, totalListaDetalleAux, tiempoMeses);
			listaFacturas = mVentas.findAllFacturas();
			System.out.println("Cuotas Generadas");
			JSFUtil.crearMensajeINFO("Contrato Generado");
			return "menuFactura";
		} catch (Exception e) {
			// TODO: handle exception
			return "";
		}

	}

	public void prueba() {
		System.out.println("mostrando descriocon de tipo pago elejido: " + idTipopago);
	}

	// Get y Set
	public DTOclientes getDtoCliente() {
		return dtoCliente;
	}

	public void setDtoCliente(DTOclientes dtoCliente) {
		this.dtoCliente = dtoCliente;
	}

	public DTOpersona getDtoPersonas() {
		return dtoPersonas;
	}

	public void setDtoPersonas(DTOpersona dtoPersonas) {
		this.dtoPersonas = dtoPersonas;
	}

	public TblCliente getCliente() {
		return cliente;
	}

	public void setCliente(TblCliente cliente) {
		this.cliente = cliente;
	}

	public TblCliente getPersona() {
		return persona;
	}

	public void setPersona(TblCliente persona) {
		this.persona = persona;
	}

	public String getBuscarCliente() {
		return buscarCliente;
	}

	public void setBuscarCliente(String buscarCliente) {
		this.buscarCliente = buscarCliente;
	}

	public List<TblCategoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<TblCategoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public int getIdCategoriaPrincipal() {
		return idCategoriaPrincipal;
	}

	public void setIdCategoriaPrincipal(int idCategoriaPrincipal) {
		this.idCategoriaPrincipal = idCategoriaPrincipal;
	}

	public int getNumcolumns() {
		return numcolumns;
	}

	public void setNumcolumns(int numcolumns) {
		this.numcolumns = numcolumns;
	}

	public List<TblCategoria> getListaSubCategorias() {
		return listaSubCategorias;
	}

	public void setListaSubCategorias(List<TblCategoria> listaSubCategorias) {
		this.listaSubCategorias = listaSubCategorias;
	}

	public List<TblCategoria> getListaSub_Categorias() {
		return listaSub_Categorias;
	}

	public void setListaSub_Categorias(List<TblCategoria> listaSub_Categorias) {
		this.listaSub_Categorias = listaSub_Categorias;
	}

	public int getIdSubCategoriaPrincipal() {
		return idSubCategoriaPrincipal;
	}

	public void setIdSubCategoriaPrincipal(int idSubCategoriaPrincipal) {
		this.idSubCategoriaPrincipal = idSubCategoriaPrincipal;
	}

	public List<SelectItem> getListasSelectItemSubcategorias() {
		return listasSelectItemSubcategorias;
	}

	public void setListasSelectItemSubcategorias(List<SelectItem> listasSelectItemSubcategorias) {
		this.listasSelectItemSubcategorias = listasSelectItemSubcategorias;
	}

	public SelectItem[] getExample() {
		return example;
	}

	public void setExample(SelectItem[] example) {
		this.example = example;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public List<TblProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<TblProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public int getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

	public DTOproducto getDtoProducto() {
		return dtoProducto;
	}

	public void setDtoProducto(DTOproducto dtoProducto) {
		this.dtoProducto = dtoProducto;
	}

	public List<TblCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<TblCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public TblCliente getEdicionCliente() {
		return edicionCliente;
	}

	public void setEdicionCliente(TblCliente edicionCliente) {
		this.edicionCliente = edicionCliente;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public Date getFechaActual() {
		return fechaActual;
	}

	public void setFechaActual(Date fechaActual) {
		this.fechaActual = fechaActual;
	}

	public List<TblTipoPago> getListapagos() {
		return listapagos;
	}

	public void setListapagos(List<TblTipoPago> listapagos) {
		this.listapagos = listapagos;
	}

	public int getNumFactPro() {
		return numFactPro;
	}

	public void setNumFactPro(int numFactPro) {
		this.numFactPro = numFactPro;
	}

	public BigDecimal getPrecioseleccionado() {
		return precioseleccionado;
	}

	public void setPrecioseleccionado(BigDecimal precioseleccionado) {
		this.precioseleccionado = precioseleccionado;
	}

	public int getCantidadseleccionada() {
		return cantidadseleccionada;
	}

	public void setCantidadseleccionada(int cantidadseleccionada) {
		this.cantidadseleccionada = cantidadseleccionada;
	}

	public List<TblTDocumento> getListaDocumentos() {
		return listaDocumentos;
	}

	public void setListaDocumentos(List<TblTDocumento> listaDocumentos) {
		this.listaDocumentos = listaDocumentos;
	}

	public TblFactPro getCabecera() {
		return cabecera;
	}

	public void setCabecera(TblFactPro cabecera) {
		this.cabecera = cabecera;
	}

	public List<BigDecimal> getListaprecios() {
		return listaprecios;
	}

	public void setListaprecios(List<BigDecimal> listaprecios) {
		this.listaprecios = listaprecios;
	}

	public List<TblDetalleFactura> getListaDetalleFactura() {
		return listaDetalleFactura;
	}

	public void setListaDetalleFactura(List<TblDetalleFactura> listaDetalleFactura) {
		this.listaDetalleFactura = listaDetalleFactura;
	}

	public BigDecimal getSubtotalListaDetalle() {
		return subtotalListaDetalle;
	}

	public void setSubtotalListaDetalle(BigDecimal subtotalListaDetalle) {
		this.subtotalListaDetalle = subtotalListaDetalle;
	}

	public BigDecimal getTotalListaDetalle() {
		return totalListaDetalle;
	}

	public void setTotalListaDetalle(BigDecimal totalListaDetalle) {
		this.totalListaDetalle = totalListaDetalle;
	}

	public BigDecimal getIvaListaDetalle() {
		return ivaListaDetalle;
	}

	public void setIvaListaDetalle(BigDecimal ivaListaDetalle) {
		this.ivaListaDetalle = ivaListaDetalle;
	}

	public int getIdTipopago() {
		return idTipopago;
	}

	public void setIdTipopago(int idTipopago) {
		this.idTipopago = idTipopago;
	}

	public int getIdTipodoc() {
		return idTipodoc;
	}

	public void setIdTipodoc(int idTipodoc) {
		this.idTipodoc = idTipodoc;
	}

	public List<TblFactPro> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(List<TblFactPro> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public List<TblFactPro> getListaProformas() {
		return listaProformas;
	}

	public void setListaProformas(List<TblFactPro> listaProformas) {
		this.listaProformas = listaProformas;
	}

	public List<TblTasaNominal> getListaTasaNominal() {
		return listaTasaNominal;
	}

	public void setListaTasaNominal(List<TblTasaNominal> listaTasaNominal) {
		this.listaTasaNominal = listaTasaNominal;
	}

	public BigDecimal getPorcentajeSeleccionado() {
		return porcentajeSeleccionado;
	}

	public void setPorcentajeSeleccionado(BigDecimal porcentajeSeleccionado) {
		this.porcentajeSeleccionado = porcentajeSeleccionado;
	}

	public int getTiempoMeses() {
		return tiempoMeses;
	}

	public void setTiempoMeses(int tiempoMeses) {
		this.tiempoMeses = tiempoMeses;
	}

	public String getDescripcinonTipopago() {
		return descripcinonTipopago;
	}

	public void setDescripcinonTipopago(String descripcinonTipopago) {
		this.descripcinonTipopago = descripcinonTipopago;
	}

	public String getModBotonGuardarVenta() {
		return modBotonGuardarVenta;
	}

	public void setModBotonGuardarVenta(String modBotonGuardarVenta) {
		this.modBotonGuardarVenta = modBotonGuardarVenta;
	}

	public String getModBotonCuotas() {
		return modBotonCuotas;
	}

	public void setModBotonCuotas(String modBotonCuotas) {
		this.modBotonCuotas = modBotonCuotas;
	}

	public TblTasaNominal getNuevaTasa() {
		return NuevaTasa;
	}

	public void setNuevaTasa(TblTasaNominal nuevaTasa) {
		NuevaTasa = nuevaTasa;
	}

	public String getModBotonInf() {
		return modBotonInf;
	}

	public void setModBotonInf(String modBotonInf) {
		this.modBotonInf = modBotonInf;
	}

}
