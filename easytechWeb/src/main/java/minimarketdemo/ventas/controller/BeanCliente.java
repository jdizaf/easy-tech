package minimarketdemo.ventas.controller;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.ventas.managers.ManagerCliente;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;






@Named
@SessionScoped
public class BeanCliente implements Serializable {

	@EJB
	private ManagerCliente mCliente;
	private TblCliente clienteNuevo;
	private TblPersona personaNuevo;
	private TblCliente edicionCliente;
	private List<TblPersona> listaPersona;
	private List<TblCliente> listaCliente;
	private List<TblCliente> listaClienteAll;
	

	public BeanCliente() {
		
	}
	
	@PostConstruct
	public void inicializar() {
		clienteNuevo=new TblCliente();
		personaNuevo=new TblPersona();
		listaPersona = mCliente.findAllPersona();
		listaCliente =mCliente.findAllCliente();
		listaClienteAll=mCliente.findAllClienteAll();
		
	}
	
	//Consultar Clientes
	public void actionFindAllCliente() throws Exception {
		listaCliente = mCliente.findAllCliente();
		listaClienteAll=mCliente.findAllClienteAll();
	}
	
	//Editar Cliente
	public void actionSeleccionarEdicionCliente(TblCliente clientes) {
		edicionCliente=clientes;
	}
	//Actualizar Cliente
	public void actualizarRegistroCliente() {
		try {
			mCliente.updateRegistroCliente(edicionCliente);
			listaCliente = mCliente.findAllCliente();
			listaClienteAll=mCliente.findAllClienteAll();
			edicionCliente=new TblCliente();
			JSFUtil.crearMensajeINFO("Cliente actualizado.");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	//Eliminar cliente en estado logico
	public void eliminarRegistroClientelogico(TblCliente clientes) throws Exception {
		try {		
			mCliente.deleteRegistroClientelogico(clientes);
			JSFUtil.crearMensajeINFO("Cliente Inactivo.");
			listaCliente = mCliente.findAllCliente();
			listaClienteAll=mCliente.findAllClienteAll();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	//Actulizar Cliente a True
	public void updateClientelogico(TblCliente clientes) throws Exception {
		try {
			mCliente.updateClientelogico(clientes);
			JSFUtil.crearMensajeINFO("Cliente Activo.");
			listaCliente = mCliente.findAllCliente();
			listaClienteAll=mCliente.findAllClienteAll();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	// Registro de Clientes
	public String actionListenerInsertarCliente() {
		try {
			
			mCliente.insertarPersona(personaNuevo);
			listaPersona = mCliente.findAllPersona();
			personaNuevo=new TblPersona();
			
			listaCliente = mCliente.findAllCliente();
			listaClienteAll=mCliente.findAllClienteAll();
			return "menu";
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
			return "";
		}
	}
	
	
	// reporte clientes
	
	public String actionReporteClientes() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("cliente/reporte_clientes.jasper");
		System.out.println("RUTA: " + ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=Reporte_clientes.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	public TblCliente getClienteNuevo() {
		return clienteNuevo;
	}

	public void setClienteNuevo(TblCliente clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}

	public TblPersona getPersonaNuevo() {
		return personaNuevo;
	}

	public void setPersonaNuevo(TblPersona personaNuevo) {
		this.personaNuevo = personaNuevo;
	}

	public List<TblPersona> getListaPersona() {
		return listaPersona;
	}

	public void setListaPersona(List<TblPersona> listaPersona) {
		this.listaPersona = listaPersona;
	}

	public List<TblCliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<TblCliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public TblCliente getEdicionCliente() {
		return edicionCliente;
	}

	public void setEdicionCliente(TblCliente edicionCliente) {
		this.edicionCliente = edicionCliente;
	}

	public List<TblCliente> getListaClienteAll() {
		return listaClienteAll;
	}

	public void setListaClienteAll(List<TblCliente> listaClienteAll) {
		this.listaClienteAll = listaClienteAll;
	}
	
	

	

	
	
	
	
	
	
	/*public void actionFindClienteByCedula() throws Exception {
		listaCliente = mCliente.findByCedula(cedulaCliente);
		clienteNuevo.setCliCedula(cedulaCliente);
	}
	
	public void actionFindAllCliente() throws Exception {
		listaCliente = mCliente.findAllCliente();
	}
	
	public void actionSeleccionarEdicionCliente(Cliente clientes) {
		edicionCliente=clientes;
	}
	
	public void actualizarRegistroCliente() {
		try {
			mCliente.updateRegistroCliente(edicionCliente);
			JSFUtil.crearMensajeINFO("Cliente actualizado.");
			listaCliente = mCliente.findAllCliente();
			edicionCliente=new Cliente();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}
	
	
	public void eliminarRegistroClientelogico(Cliente clientes) throws Exception {
		try {		
			mCliente.deleteRegistroClientelogico(clientes);
			JSFUtil.crearMensajeINFO("Cliente Eliminado.");
			listaCliente = mCliente.findAllCliente();
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public Cliente getClienteNuevo() {
		return clienteNuevo;
	}

	public void setClienteNuevo(Cliente clienteNuevo) {
		this.clienteNuevo = clienteNuevo;
	}

	public Cliente getEdicionCliente() {
		return edicionCliente;
	}

	public void setEdicionCliente(Cliente edicionCliente) {
		this.edicionCliente = edicionCliente;
	}

	public List<Cliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}

	public String getCedulaCliente() {
		return cedulaCliente;
	}

	public void setCedulaCliente(String cedulaCliente) {
		this.cedulaCliente = cedulaCliente;
	}*/
	
}
