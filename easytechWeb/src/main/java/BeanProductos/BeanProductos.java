package BeanProductos;

import java.io.ByteArrayOutputStream;

import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.model.file.UploadedFile;

import easytech.model.productos.dtos.DTOproductos;
import easytech.model.productos.managers.ManagerProductos;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.*;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import javax.faces.context.FacesContext;

import org.primefaces.model.StreamedContent;

@Named
@SessionScoped
public class BeanProductos implements Serializable {
	private static final long serialVersionUID = 1L;
	@EJB
	private ManagerProductos managerProductos;

	//////// DTO
	DTOproductos dtoProductos;

	// variables extras
	private int idCategoriaPrincipal;
	private int idSubCategoriaPrincipal;
	private int idSubCategoria;
	private int idSub_Categoria;

	private int idCategoriaPrincipalEdicion;
	private int idSub_CategoriaEdicion;
	private int idSubCategoriaEdicion;

	//// Listas
	private List<TblCategoria> listaCategorias;
	private List<TblCategoria> listaSubCategorias;
	private List<TblCategoria> listaSub_Categorias;
	private List<TblProducto> listaProductos;

	// Variables extras
	private int numcolumns;
	private String modEstadoVista;
	private String modEstadoVistaComboBoxCategoriaProductos;
	private String modEstadoTituloCategoria;
	private int idCategoriaFinal;
	private StreamedContent imgenReceptor;
	private String vistaEdicionprecio;
	private int numColumnasEdicion;
	private int idCategoriaPrincipalE;
	private int idSub_CategoriaE;
	private int idSubCategoriaE;

	////////////////////
	private double precio1;
	private double precio2;
	private double precio3;
	private double porcentaje1;
	private double porcentaje2;
	private double porcentaje3;
	private double ganancia1;
	private double ganancia2;
	private double ganancia3;
	private double precioProducto;
	private List<Double> listaNuevoPrecios;

	private List<SelectItem> listasSelectItemSubcategorias;
	private List<TblCategoria> listaRespaldoCategorias;
	private List<TblProducto> listaProductosDescomponido;
	private SelectItem[] example;

	//////// Eztra TBLS
	TblProducto tblProducto;
	TblProducto tblEdicionProducto;

	// Variables Imagenes
	UploadedFile imagen;

	public BeanProductos() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void Inicializar() {

		listaCategorias = managerProductos.findCategoriaPrincipal();
		idCategoriaPrincipal = 0;
		numcolumns = 2;
		listasSelectItemSubcategorias = new ArrayList<SelectItem>();
		modEstadoVista = "Display:none";
		modEstadoVistaComboBoxCategoriaProductos = "Display:none";
		modEstadoTituloCategoria = "Descripción:";
		dtoProductos = new DTOproductos();
		listaProductos = managerProductos.findAllProductos();
		tblEdicionProducto = new TblProducto();
		System.out.println(idCategoriaPrincipal);
		vistaEdicionprecio = "Display:none";
		numColumnasEdicion = 2;

	}

	public void actionListenerRegistrarProducto() {
		listaSubCategorias = managerProductos.findSubCategorias(idCategoriaPrincipal);
		listaSub_Categorias = managerProductos.findSubCategorias(idSubCategoria);
		try {

			if (listaSub_Categorias.size() == 0) {
				dtoProductos.setIdCategoria(idSubCategoria);
				System.out.println("a1");
			} else {
				dtoProductos.setIdCategoria(idSub_Categoria);
				System.out.println("a2");

			}
			System.out.println("Iamgen: " + imagen);
			InputStream input = imagen.getInputStream();
			System.out.println("b");
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			System.out.println("c");
			byte[] buffer = new byte[1024];
			System.out.println("d");
			for (int i = 0; (i = input.read(buffer)) > 0;) {
				System.out.println("e" + i);
				output.write(buffer, 0, i);
			}
			System.out.println("f");
			dtoProductos.setImagen(output.toByteArray());
			System.out.println("g");
			managerProductos.registrarProducto(dtoProductos);
			System.out.println("h");
			dtoProductos = new DTOproductos();

			listaProductos = managerProductos.findAllProductos();

			JSFUtil.crearMensajeINFO("Producto agregado");
			idCategoriaPrincipal = idSub_Categoria = idSubCategoria = 0;

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR("Error");
		}

	}

	public void actionListenerEditarProducto(TblProducto tblProducto) {

		try {
			String[] parts;
			tblEdicionProducto = managerProductos.mostrarProducto(tblProducto.getIdProducto());
			int precioCompraInt = tblEdicionProducto.getPrecioCompra().intValue();
			precioProducto = tblProducto.getPrecioCompra().doubleValue();
			String description = tblEdicionProducto.getDescripcion();
			parts = description.split(", ");
			if (parts.length > 1) {
				dtoProductos.setDescripcion_nombre(parts[0]);
				dtoProductos.setMarca(parts[1]);
				dtoProductos.setModelo(parts[2]);
				dtoProductos.setSerie(parts[3]);

			} else {
				dtoProductos.setDescripcion_nombre(description);
				dtoProductos.setMarca("");
				dtoProductos.setModelo("");
				dtoProductos.setSerie("");
			}

			if (precioCompraInt == 0) {
				vistaEdicionprecio = "Display:none";
				numColumnasEdicion = 2;

			} else {
				vistaEdicionprecio = "Display:block";
				numColumnasEdicion = 4;
			}

		} catch (Exception e) {

		}
	}

	public void actionListenerEjecutarEditarProducto() {

		try {
			System.out.println("1");
			int precioCompraInt = tblEdicionProducto.getPrecioCompra().intValue();
			System.out.println("2");
//			System.out.println(tblProducto.getPrecioCompra().doubleValue());
//			precioProducto = tblProducto.getPrecioCompra().doubleValue();

			System.out.println("3");
			if (dtoProductos.getMarca() == "" || dtoProductos.getModelo() == "" || dtoProductos.getSerie() == "") {
//				System.out.println("4");

				tblEdicionProducto.setDescripcion(dtoProductos.getDescripcion_nombre());

			} else {
				System.out.println("5");
				tblEdicionProducto.setDescripcion(dtoProductos.getDescripcion_nombre() + ", " + dtoProductos.getMarca()
						+ ", " + dtoProductos.getModelo() + ", " + dtoProductos.getSerie());

			}
			System.out.println("Precio producto: " + precioProducto);
			if (precioProducto == 0) {

				tblEdicionProducto.setPrecioCompra(new BigDecimal(0));
				tblEdicionProducto.setPrecioVenta1(new BigDecimal(0));
				tblEdicionProducto.setPrecioVenta2(new BigDecimal(0));
				tblEdicionProducto.setPrecioVenta3(new BigDecimal(0));
			} else {
				tblEdicionProducto.setPrecioCompra(new BigDecimal(precioProducto));
				tblEdicionProducto.setPrecioVenta1(new BigDecimal(precio1));
				tblEdicionProducto.setPrecioVenta2(new BigDecimal(precio2));
				tblEdicionProducto.setPrecioVenta3(new BigDecimal(precio3));
			}
			TblCategoria tblCategoria = new TblCategoria();
			if (modEstadoVistaComboBoxCategoriaProductos.equals("Display:none")) {
				tblCategoria.setIdCategoria(idSubCategoria);
			} else if (modEstadoVistaComboBoxCategoriaProductos.equals("Display:block")) {
				tblCategoria.setIdCategoria(idSub_CategoriaE);
			}
			tblEdicionProducto.setEstado(true);
			tblEdicionProducto.setImagen(null);

			managerProductos.ActualizarProducto(tblEdicionProducto);
			listaProductos = managerProductos.findAllProductos();
			tblEdicionProducto = new TblProducto();
			dtoProductos = new DTOproductos();
		
			System.out.println("Todo valiod elu putas ");
			JSFUtil.crearMensajeINFO("Editado");
		} catch (Exception e) {
			JSFUtil.crearMensajeINFO("Error");
		}

	}

	public void actionEjecutarCategorias() {
		idSubCategoria = 0;
		modEstadoVistaComboBoxCategoriaProductos = "Display:none";
		modEstadoVista = "Display:none";
		modEstadoTituloCategoria = "Descripción: ";
		modEstadoVistaComboBoxCategoriaProductos = "Display:none";
		listaCategorias = managerProductos.findCategoriaPrincipal();
		listaSubCategorias = managerProductos.findSubCategorias(idCategoriaPrincipal);
		System.out.println("Categoria principal que busca: " + idCategoriaPrincipal);

	}

	public void actionEjecutarSubcategorias() {
		listaSub_Categorias = managerProductos.findSubCategorias(idSubCategoria);
		System.out.println("SubCategoria principal que busca: " + idSubCategoria);
		idCategoriaFinal = idSubCategoria;
		dtoProductos.setIdCategoria(idCategoriaFinal);

		if (listaSub_Categorias.size() == 0) {
			modEstadoVistaComboBoxCategoriaProductos = "Display:none";
			modEstadoVista = "Display:none";
			modEstadoTituloCategoria = "Descripción: ";
		}

		else {
			modEstadoVistaComboBoxCategoriaProductos = "Display:block";
			modEstadoVista = "Display:block";

			modEstadoTituloCategoria = "Nombre: ";
			idCategoriaFinal = idSub_Categoria;
			dtoProductos.setIdCategoria(idCategoriaFinal);
		}
	}

	public void actionListenerEliminarProducto(TblProducto tblProducto) {
		try {
			System.out.println("Id: " + tblProducto.getIdProducto());
			managerProductos.EliminarProducto(tblProducto.getIdProducto());
			JSFUtil.crearMensajeINFO("Producto Eliminado" + tblProducto.getIdProducto());

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR("Error");

		}

	}

	public String actionReporteProductos() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("productos/reporte_productos.jasper");
		System.out.println("RUTA: " + ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=Reporte_productos.pdf");
		response.setContentType("application/pdf");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	public void actionListenerCalcularPrecios() {
		try {

			precio1 = (precioProducto * (porcentaje1 / 100)) + precioProducto;
			precio2 = (precioProducto * (porcentaje2 / 100)) + precioProducto;
			precio3 = (precioProducto * (porcentaje3 / 100)) + precioProducto;

			ganancia1 = precio1 - precioProducto;
			ganancia2 = precio2 - precioProducto;
			ganancia3 = precio3 - precioProducto;

			listaNuevoPrecios.clear();

			listaNuevoPrecios = managerProductos.calcularPreciosProducto(precioProducto, porcentaje1, porcentaje2,
					porcentaje3);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public List<TblCategoria> getListaCategorias() {
		return listaCategorias;
	}

	public void setListaCategorias(List<TblCategoria> listaCategorias) {
		this.listaCategorias = listaCategorias;
	}

	public int getIdCategoriaPrincipal() {
		return idCategoriaPrincipal;
	}

	public void setIdCategoriaPrincipal(int idCategoriaPrincipal) {
		this.idCategoriaPrincipal = idCategoriaPrincipal;
	}

	public int getNumcolumns() {
		return numcolumns;
	}

	public void setNumcolumns(int numcolumns) {
		this.numcolumns = numcolumns;
	}

	public List<TblCategoria> getListaSubCategorias() {
		return listaSubCategorias;
	}

	public void setListaSubCategorias(List<TblCategoria> listaSubCategorias) {
		this.listaSubCategorias = listaSubCategorias;
	}

	public List<TblCategoria> getListaSub_Categorias() {
		return listaSub_Categorias;
	}

	public void setListaSub_Categorias(List<TblCategoria> listaSub_Categorias) {
		this.listaSub_Categorias = listaSub_Categorias;
	}

	public int getIdSubCategoriaPrincipal() {
		return idSubCategoriaPrincipal;
	}

	public void setIdSubCategoriaPrincipal(int idSubCategoriaPrincipal) {
		this.idSubCategoriaPrincipal = idSubCategoriaPrincipal;
	}

	public List<SelectItem> getListasSelectItemSubcategorias() {
		return listasSelectItemSubcategorias;
	}

	public void setListasSelectItemSubcategorias(List<SelectItem> listasSelectItemSubcategorias) {
		this.listasSelectItemSubcategorias = listasSelectItemSubcategorias;
	}

	public SelectItem[] getExample() {
		return example;
	}

	public void setExample(SelectItem[] example) {
		this.example = example;
	}

	public String getModEstadoVista() {
		return modEstadoVista;
	}

	public void setModEstadoVista(String modEstadoVista) {
		this.modEstadoVista = modEstadoVista;
	}

	public List<TblCategoria> getListaRespaldoCategorias() {
		return listaRespaldoCategorias;
	}

	public void setListaRespaldoCategorias(List<TblCategoria> listaRespaldoCategorias) {
		this.listaRespaldoCategorias = listaRespaldoCategorias;
	}

	public int getIdSubCategoria() {
		return idSubCategoria;
	}

	public void setIdSubCategoria(int idSubCategoria) {
		this.idSubCategoria = idSubCategoria;
	}

	public int getIdSub_Categoria() {
		return idSub_Categoria;
	}

	public void setIdSub_Categoria(int idSub_Categoria) {
		this.idSub_Categoria = idSub_Categoria;
	}

	public String getModEstadoVistaComboBoxCategoriaProductos() {
		return modEstadoVistaComboBoxCategoriaProductos;
	}

	public void setModEstadoVistaComboBoxCategoriaProductos(String modEstadoVistaComboBoxCategoriaProductos) {
		this.modEstadoVistaComboBoxCategoriaProductos = modEstadoVistaComboBoxCategoriaProductos;
	}

	public String getModEstadoTituloCategoria() {
		return modEstadoTituloCategoria;
	}

	public void setModEstadoTituloCategoria(String modEstadoTituloCategoria) {
		this.modEstadoTituloCategoria = modEstadoTituloCategoria;
	}

	public DTOproductos getDtoProductos() {
		return dtoProductos;
	}

	public void setDtoProductos(DTOproductos dtoProductos) {
		this.dtoProductos = dtoProductos;
	}

	public List<TblProducto> getListaProductos() {
		return listaProductos;
	}

	public void setListaProductos(List<TblProducto> listaProductos) {
		this.listaProductos = listaProductos;
	}

	public List<TblProducto> getListaProductosDescomponido() {
		return listaProductosDescomponido;
	}

	public void setListaProductosDescomponido(List<TblProducto> listaProductosDescomponido) {
		this.listaProductosDescomponido = listaProductosDescomponido;
	}

	public int getIdCategoriaFinal() {
		return idCategoriaFinal;
	}

	public void setIdCategoriaFinal(int idCategoriaFinal) {
		this.idCategoriaFinal = idCategoriaFinal;
	}

	public TblProducto getTblEdicionProducto() {
		return tblEdicionProducto;
	}

	public void setTblEdicionProducto(TblProducto tblEdicionProducto) {
		this.tblEdicionProducto = tblEdicionProducto;
	}

	public int getIdCategoriaPrincipalEdicion() {
		return idCategoriaPrincipalEdicion;
	}

	public void setIdCategoriaPrincipalEdicion(int idCategoriaPrincipalEdicion) {
		this.idCategoriaPrincipalEdicion = idCategoriaPrincipalEdicion;
	}

	public int getIdSub_CategoriaEdicion() {
		return idSub_CategoriaEdicion;
	}

	public void setIdSub_CategoriaEdicion(int idSub_CategoriaEdicion) {
		this.idSub_CategoriaEdicion = idSub_CategoriaEdicion;
	}

	public int getIdSubCategoriaEdicion() {
		return idSubCategoriaEdicion;
	}

	public void setIdSubCategoriaEdicion(int idSubCategoriaEdicion) {
		this.idSubCategoriaEdicion = idSubCategoriaEdicion;
	}

	public UploadedFile getImagen() {
		return imagen;
	}

	public void setImagen(UploadedFile imagen) {
		this.imagen = imagen;
	}

	public StreamedContent getImgenReceptor() {
		return imgenReceptor;
	}

	public void setImgenReceptor(StreamedContent imgenReceptor) {
		this.imgenReceptor = imgenReceptor;
	}

	public String getVistaEdicionprecio() {
		return vistaEdicionprecio;
	}

	public void setVistaEdicionprecio(String vistaEdicionprecio) {
		this.vistaEdicionprecio = vistaEdicionprecio;
	}

	public int getNumColumnasEdicion() {
		return numColumnasEdicion;
	}

	public void setNumColumnasEdicion(int numColumnasEdicion) {
		this.numColumnasEdicion = numColumnasEdicion;
	}

	public double getPrecio1() {
		return precio1;
	}

	public void setPrecio1(double precio1) {
		this.precio1 = precio1;
	}

	public double getPrecio2() {
		return precio2;
	}

	public void setPrecio2(double precio2) {
		this.precio2 = precio2;
	}

	public double getPrecio3() {
		return precio3;
	}

	public void setPrecio3(double precio3) {
		this.precio3 = precio3;
	}

	public double getPorcentaje1() {
		return porcentaje1;
	}

	public void setPorcentaje1(double porcentaje1) {
		this.porcentaje1 = porcentaje1;
	}

	public double getPorcentaje2() {
		return porcentaje2;
	}

	public void setPorcentaje2(double porcentaje2) {
		this.porcentaje2 = porcentaje2;
	}

	public double getPorcentaje3() {
		return porcentaje3;
	}

	public void setPorcentaje3(double porcentaje3) {
		this.porcentaje3 = porcentaje3;
	}

	public double getGanancia1() {
		return ganancia1;
	}

	public void setGanancia1(double ganancia1) {
		this.ganancia1 = ganancia1;
	}

	public double getGanancia2() {
		return ganancia2;
	}

	public void setGanancia2(double ganancia2) {
		this.ganancia2 = ganancia2;
	}

	public double getGanancia3() {
		return ganancia3;
	}

	public void setGanancia3(double ganancia3) {
		this.ganancia3 = ganancia3;
	}

	public double getPrecioProducto() {
		return precioProducto;
	}

	public void setPrecioProducto(double precioProducto) {
		this.precioProducto = precioProducto;
	}

	public List<Double> getListaNuevoPrecios() {
		return listaNuevoPrecios;
	}

	public void setListaNuevoPrecios(List<Double> listaNuevoPrecios) {
		this.listaNuevoPrecios = listaNuevoPrecios;
	}

	public int getIdCategoriaPrincipalE() {
		return idCategoriaPrincipalE;
	}

	public void setIdCategoriaPrincipalE(int idCategoriaPrincipalE) {
		this.idCategoriaPrincipalE = idCategoriaPrincipalE;
	}

	public int getIdSub_CategoriaE() {
		return idSub_CategoriaE;
	}

	public void setIdSub_CategoriaE(int idSub_CategoriaE) {
		this.idSub_CategoriaE = idSub_CategoriaE;
	}

	public int getIdSubCategoriaE() {
		return idSubCategoriaE;
	}

	public void setIdSubCategoriaE(int idSubCategoriaE) {
		this.idSubCategoriaE = idSubCategoriaE;
	}

}