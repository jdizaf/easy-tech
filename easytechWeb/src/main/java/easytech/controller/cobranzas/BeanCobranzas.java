package easytech.controller.cobranzas;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;

import easytech.model.cobranzas.dtos.DTOpersona;
import easytech.model.cobranzas.managers.ManagerCobranzas;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.TblAcuerdo;
import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblCuota;
import minimarketdemo.model.core.entities.TblTasaNominal;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

@Named
@SessionScoped
public class BeanCobranzas implements Serializable {

	private static final long serialVersionUID = 1L;

	@EJB
	private ManagerCobranzas mCobranzas;

	/// Listas
	private List<TblCliente> listaClientes;
	private List<TblAcuerdo> listaAcuerdos;
	private List<TblTasaNominal> listaTNominal;
	private List<TblAcuerdo> listaAcuerdosByCliente;
	private List<TblCuota> listaCuotas;
	private List<TblAcuerdo> listaAcuerdoClientes;

//	private List<TblCliente> listaClienteConAcuerdos;

	// DTOS
	private DTOpersona dtoPersonas;

//VARIABLES
	private int cedula;
	private int idCliente;
	private int idAcuerdoCliente;
	private double abono;

	// Tablas
	private TblAcuerdo tblAcuerdo;
	private TblCuota tblCuotaSeleccionado;
	private int acuerdoSeleccionado;
	private double porcentaje;

	@PostConstruct
	public void inicializar() {
		listaClientes = mCobranzas.findAllAcuerdoClientes();
		listaAcuerdos = mCobranzas.findAllAcuerdosClientes();
		listaAcuerdoClientes = new ArrayList<TblAcuerdo>();
		listaTNominal = mCobranzas.findAllNominal();
		acuerdoSeleccionado = 1;

	}

	public void actionRealizarPago() {
		try {
			mCobranzas.actualizarCuota(tblCuotaSeleccionado);
			listaCuotas = mCobranzas.ObtenerCuotasAcuerdo(tblCuotaSeleccionado.getTblAcuerdo().getIdAcuerdo());
			JSFUtil.crearMensajeINFO("Pago registrado");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR("Pago no registrado");
		}

	}

	public void actionListenerProbar() {
		TblCliente tblCliente = new TblCliente();
		tblCliente.setIdCliente(1);
		mCobranzas.GenerarCuotasCreditoContrato(tblCliente, 25, 700, 6);
	}

	public void actionListenerSleccionarCuota(TblCuota tblCuota) {
		tblCuotaSeleccionado = tblCuota;
	}

	public void actionListenerAbonar() {
		try {
			if (abono == 0) {
				JSFUtil.crearMensajeERROR("El abono no puede ser 0");
			} else {
				mCobranzas.realizarAbonoCuota(tblCuotaSeleccionado, abono);
				JSFUtil.crearMensajeINFO("Abono registrado");
			}

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR("No se puede abonar");
		}

	}

	public void actionObtenerDatosCliente() {

		dtoPersonas = mCobranzas.findClientByCedula(idCliente);
		listaAcuerdoClientes = mCobranzas.getIdsAcuerdosByCliente(idCliente);
		listaCuotas = mCobranzas.ObtenerCuotasAcuerdo(idAcuerdoCliente);
	}

	public void actionObtenerDatosContrato(TblAcuerdo tblAcuerdo) {
		tblAcuerdo = mCobranzas.ObtenerDatosAcuerdo(tblAcuerdo.getIdAcuerdo());
		listaCuotas = mCobranzas.ObtenerCuotasAcuerdo(tblAcuerdo.getIdAcuerdo());
	}

	// JASPER

	// si vale
	public String actionReporteAcuerdos(TblAcuerdo acuerdo) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		int acuerdoSeleccionado = acuerdo.getIdAcuerdo();
		parametros.put("idAcuerdo", acuerdoSeleccionado);
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("cobranzas/reporte_acuerdos_2.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_acuerdos.pdf");
		response.setContentType("application/pdf");
		System.out.println("Reporte factura");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	public String actionReporteAbonos(TblAcuerdo acuerdo) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		int acuerdoSeleccionado = acuerdo.getIdAcuerdo();
		parametros.put("idAcuerdo", acuerdoSeleccionado);
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("cobranzas/reporte_abonos.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_abonos.pdf");
		response.setContentType("application/pdf");
		System.out.println("Reporte factura");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}

	

	public int getCedula() {
		return cedula;
	}

	public double getPorcentaje() {
		return porcentaje;
	}

	public void setPorcentaje(double porcentaje) {
		this.porcentaje = porcentaje;
	}

	public List<TblTasaNominal> getListaTNominal() {
		return listaTNominal;
	}

	public void setListaTNominal(List<TblTasaNominal> listaTNominal) {
		this.listaTNominal = listaTNominal;
	}

	public void setCedula(int cedula) {
		this.cedula = cedula;
	}

	public List<TblCliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(List<TblCliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public DTOpersona getDtoPersonas() {
		return dtoPersonas;
	}

	public void setDtoPersonas(DTOpersona dtoPersonas) {
		this.dtoPersonas = dtoPersonas;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public int getIdAcuerdoCliente() {
		return idAcuerdoCliente;
	}

	public void setIdAcuerdoCliente(int idAcuerdoCliente) {
		this.idAcuerdoCliente = idAcuerdoCliente;
	}

	public List<TblAcuerdo> getListaAcuerdoClientes() {
		return listaAcuerdoClientes;
	}

	public void setListaAcuerdoClientes(List<TblAcuerdo> listaAcuerdoClientes) {
		this.listaAcuerdoClientes = listaAcuerdoClientes;
	}

	public List<TblAcuerdo> getListaAcuerdos() {
		return listaAcuerdos;
	}

	public void setListaAcuerdos(List<TblAcuerdo> listaAcuerdos) {
		this.listaAcuerdos = listaAcuerdos;
	}

	public TblAcuerdo getTblAcuerdo() {
		return tblAcuerdo;
	}

	public void setTblAcuerdo(TblAcuerdo tblAcuerdo) {
		this.tblAcuerdo = tblAcuerdo;
	}

	public List<TblAcuerdo> getListaAcuerdosByCliente() {
		return listaAcuerdosByCliente;
	}

	public void setListaAcuerdosByCliente(List<TblAcuerdo> listaAcuerdosByCliente) {
		this.listaAcuerdosByCliente = listaAcuerdosByCliente;
	}

	public List<TblCuota> getListaCuotas() {
		return listaCuotas;
	}

	public void setListaCuotas(List<TblCuota> listaCuotas) {
		this.listaCuotas = listaCuotas;
	}

	public TblCuota getTblCuotaSeleccionado() {
		return tblCuotaSeleccionado;
	}

	public void setTblCuotaSeleccionado(TblCuota tblCuotaSeleccionado) {
		this.tblCuotaSeleccionado = tblCuotaSeleccionado;
	}

	public double getAbono() {
		return abono;
	}

	public void setAbono(double abono) {
		this.abono = abono;
	}
}