package easytech.controller.compras;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.inject.Named;
import javax.print.attribute.standard.MediaSize.NA;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.sound.midi.Soundbank;

import org.primefaces.PrimeFaces;
import org.primefaces.convert.BigDecimalConverter;

import easytech.model.compras.managers.DTOCategoria;
import easytech.model.compras.managers.DTOCompras;
import easytech.model.compras.managers.DTODetallesCompras;
import easytech.model.compras.managers.DTOPersona;
import easytech.model.compras.managers.DTOProducto;
import easytech.model.compras.managers.DTOProveedor;
import easytech.model.compras.managers.ManagerCompras;
import minimarketdemo.controller.JSFUtil;
import minimarketdemo.model.core.entities.TblCategoria;
import minimarketdemo.model.core.entities.TblCompra;
import minimarketdemo.model.core.entities.TblDetallesCompra;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.entities.TblProducto;
import minimarketdemo.model.core.entities.TblProveedores;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

@Named
@SessionScoped
public class BeanCompras implements Serializable {

	@EJB
	private ManagerCompras managerCompras;
	private static final long serialVersionUID = 1L;

	public BeanCompras() {
		// TODO Auto-generated constructor stub
	}

	/// ingreso de personas
	private TblPersona persona = new TblPersona();
	private String cedula;
	private String nombres;
	private String apellidos;
	private String direccion;
	private String telefono;
	private String email;
	private List<TblPersona> listPersonas;
	private DTOPersona dtoPersona;
	// ingreso de proveedor
	private TblProveedores proveedor;
	private String nombreComercial;
	private List<TblProveedores> listProveedores;
	private DTOProveedor dtoProveedor;

	// actualizar proveedores
	private TblProveedores proveedorSeleccionado;

	/* variables para categoria */
	private TblCategoria categoria;
	private List<TblCategoria> listCategoria;
	private String nombreCategoria;
	private int idSubcategoria;
	private int idSub_Subcategoria;
	private TblCategoria categoriaSeleccionado;
	private List<TblCategoria> listaCategoriasB;
	private List<TblCategoria> listaSubCategorias;
	private List<SelectItem> listasSelectItemSubcategorias;
	private List<TblCategoria> listaSub_subCategorias;
	private SelectItem[] example;
	// id de busqueda para listar categorias por grupos
	private int idCategoriaPrincipal;
	private int idCategoriaPrincipalEdicion;
	private boolean esCategoriaPricipal;
	private boolean esCategoriaPricipalEdicion;
	private String displayListaSubcategoria;
	private String displayListaSubcategoriaEdicion;
	private List<TblCategoria> listaSeparada;
	
	///seccion de compra
	private List<TblProducto> listProducto;
	private TblProveedores proveedorMostrar;
	private int idProveedorMostrar,idProductoMostrar;
	private TblProducto productoMostrar;
	private int cantidadProducto;
	private double nuevoStock;
	private double precioProducto;
	private List<Double> listaNuevoPrecios;
	
	private List<TblDetallesCompra>listaDetallesCompra;
	private TblDetallesCompra detalleCompra;
	

	private TblCompra compraCab;
	private int idAuxListaProducto;
	private double subtotalListaDetalle;
	private double totalListaDetalle;
	private double ivaListaDetalle;
	private TblDetallesCompra detalleCompraSeleccionado;
	private double precio1;
	private double precio2;
	private double precio3;
	private double porcentaje1;
	private double porcentaje2;
	private double porcentaje3;
	private double ganancia1;
	private double ganancia2;
	private double ganancia3;
	private List<TblCompra> listaCompras;
	private List<TblDetallesCompra> listaDetallesByIdCompra;
	
	@PostConstruct
	public void inicializar() {
		findAllProveedores();
		proveedor = new TblProveedores();
		findAllCategorias();
		categoria = new TblCategoria();
		listProveedores = managerCompras.findAllProveedores();
		listaCategoriasB = managerCompras.findCategoriaPrincipal();
		
		idSubcategoria = 0;
		idSub_Subcategoria=0;
		idAuxListaProducto = 0;
		
		listProducto = managerCompras.findAllProductos();
		listaDetallesCompra = new ArrayList<TblDetallesCompra>();
		detalleCompra = new TblDetallesCompra();
		detalleCompraSeleccionado = new TblDetallesCompra();
		displayListaSubcategoria = "display: block";
		displayListaSubcategoriaEdicion = "display: block";
		listaCompras = managerCompras.findAllCompras();
		separarListaCategoria(listCategoria);
	}

	/*
	 * M�todos para Proveedores
	 */
	public void findAllProveedores() {

		listProveedores = managerCompras.findAllProveedores();

	}

	public void actionListenerAgregarProveedor() throws Exception {
		System.out.println("preba 1");

		dtoPersona = new DTOPersona();
		TblPersona person = new TblPersona();
		dtoPersona.setCedula(cedula);
		dtoPersona.setNombres(nombres);
		dtoPersona.setApellidos(apellidos);
		dtoPersona.setDireccion(direccion);
		dtoPersona.setTelefono(telefono);
		dtoPersona.setEmail(email);

		System.out.println("preba 6");
		managerCompras.insertarProveedor(dtoPersona, nombreComercial);
		findAllProveedores();
		dtoPersona = null;
		nombreComercial = null;
		proveedor = null;
	}

	public void actionListenerSeleccionarProveedor(TblProveedores p) {

		System.out.println("enviando proveedor" + p.getTblPersona().getCiRuc());

		proveedorSeleccionado = null;
		proveedorSeleccionado = p;
		System.out.println(proveedorSeleccionado);
	}

	public void actualizarProveedor() {
		try {
			System.out.println("mostrando proveedor: " + proveedorSeleccionado.getTblPersona().getEmail());
			managerCompras.actualizarProveedorById(proveedorSeleccionado);
			listProveedores = managerCompras.findAllProveedores();
			JSFUtil.crearMensajeINFO("Datos actualizados.");
			proveedorSeleccionado = new TblProveedores();

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void actionListenerEliminarProveedor(int id) {
		System.out.println("entrando al bean" + id);
		try {

			managerCompras.eliminarProveedor(id);
			listProveedores = managerCompras.findAllProveedores();
			JSFUtil.crearMensajeINFO("Eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	/*
	 * M�todos para Categor�a
	 */

	public void actionListenerAgregarCategoria() throws Exception {
		System.out.println("mirando el valor del toogle: " + esCategoriaPricipal );

		
		TblCategoria c = new TblCategoria();
		if (esCategoriaPricipal) {
			c.setCategoria(nombreCategoria);
			managerCompras.insertarCategoria(c);
			findAllCategorias();
		}else {
			c.setCategoria(nombreCategoria);
			TblCategoria c2= managerCompras.findCategoriaById(idCategoriaPrincipal);
			c.setTblCategoria(c2);
			managerCompras.insertarCategoria(c);
			findAllCategorias();
		}
	}

	public void findAllCategorias() {

		listCategoria = managerCompras.findAllCategorias();
	}

	public void actionListenerEliminarCategoria(int id) {
		System.out.println("entrando al bean eliminar categoria" + id);
		try {

			managerCompras.eliminarCategoria(id);
			listCategoria = managerCompras.findAllCategorias();
			JSFUtil.crearMensajeINFO("Eliminado correctamente");
		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();
		}
	}

	public void actualizarCategoria() {

		
		try {
			
			if (esCategoriaPricipalEdicion) {
				categoriaSeleccionado.setTblCategoria(null);
				managerCompras.actualizarCategoriaById(categoriaSeleccionado);
				findAllCategorias();
			}else {
				TblCategoria c = managerCompras.findCategoriaById(idCategoriaPrincipalEdicion);
				categoriaSeleccionado.setTblCategoria(c);
				managerCompras.actualizarCategoriaById(categoriaSeleccionado);
				findAllCategorias();
			}
			
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public void actionListenerSeleccionarCategoria(TblCategoria c) {

		System.out.println("enviando categoria" + c.getCategoria());

		categoriaSeleccionado = null;
		categoriaSeleccionado = c;
		System.out.println(categoriaSeleccionado);
		if (c.getTblCategorias() == null) {
			esCategoriaPricipalEdicion = true;
			establecerDisplayListaEdicion();
		}
	}

	public void actionBuscarCategorias() {

		try {
			listaCategoriasB = managerCompras.findCategoriaPrincipal();
			System.out.println("despues de categoria" + idCategoriaPrincipal);
			listaSubCategorias = managerCompras.findSubCategorias(idCategoriaPrincipal);
			listasSelectItemSubcategorias = new ArrayList<SelectItem>();
			System.out.println("despues de subcategoria" + listaSubCategorias + " tambien el id sub " + idSubcategoria);

			int subcsubcategoria = 0;
			for (int j = 0; j < listaSubCategorias.size(); j++) {
				SelectItemGroup g1 = new SelectItemGroup(listaSubCategorias.get(j).getCategoria());
				subcsubcategoria = listaSubCategorias.get(j).getIdCategoria();

				listaSub_subCategorias = managerCompras.findSubCategorias(subcsubcategoria);
				
				//agrega a la subcategoria para que sea seleccionable
				if (listaSub_subCategorias.size() == 0) {
					example = new SelectItem[1];
					
					for (int i = 0; i < 1; i++) {

						example[i] = new SelectItem(listaSubCategorias.get(j).getCategoria(),
								"*"+listaSubCategorias.get(j).getCategoria());
					}
					
					g1.setSelectItems(example);
				} else {
					example = new SelectItem[listaSub_subCategorias.size()];
					for (int i = 0; i < listaSub_subCategorias.size(); i++) {

						example[i] = new SelectItem(listaSub_subCategorias.get(i).getCategoria(),
								listaSub_subCategorias.get(i).getCategoria());

					}
					g1.setSelectItems(example);

				}

				listasSelectItemSubcategorias.add(g1);

			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}
	public void establecerDisplayLista() {
		if (esCategoriaPricipal == false) {
			separarListaCategoria(listCategoria);
			displayListaSubcategoria = "display: block";
			
		}else {
			
			displayListaSubcategoria = "display: none";
		}
	}
	public void establecerDisplayListaEdicion() {
		if (esCategoriaPricipalEdicion == false) {
			separarListaCategoria(listCategoria);
			displayListaSubcategoriaEdicion = "display: block";
			
		}else {
			
			displayListaSubcategoriaEdicion = "display: none";
		}
	}
	public void separarListaCategoria(List<TblCategoria> lista) {
		listaSeparada = managerCompras.separarListaCategoriasSecundarias(lista);
	}
	
	
	/*
	 * M�todos para Compras
	 */
	
	public void actionListenerMostrarDetallesProveedor() {
		proveedorMostrar = managerCompras.findProveedorById(idProveedorMostrar);
	}
	public void actionListenerMostrarDetallesProducto() {
		productoMostrar = managerCompras.findProductoById(idProductoMostrar);
		actionListenerCalcularVistaCantidad();
	}
	public void actionListenerCalcularVistaCantidad() {
		if (this.productoMostrar != null) {
			
			int totalStock = productoMostrar.getStock()+cantidadProducto;
			nuevoStock = totalStock;

		}else {
			nuevoStock =0;

		}
	}
	public void actionListenerCalcularNuevoPrecio()throws Exception {
		
		if (this.productoMostrar != null) {
			listaNuevoPrecios = managerCompras.calcularPreciosProducto(precioProducto,porcentaje1 , porcentaje2, porcentaje3);

		}else {
			nuevoStock =0;

		}
	}
	

	public void agregarListaDetalleProducto() {
		System.out.println("VErificando si hay producto mostrar " + productoMostrar);
		System.out.println("VErificando si hay cantidadad producto " + cantidadProducto);
		actionListenerCalcularPrecios();
		if (this.productoMostrar != null ){
			
			if (listaDetallesCompra.isEmpty()) {
				idAuxListaProducto = 0;
				System.out.println("Veridificando si entra cuando esta vacio");
				compraCab = new TblCompra();
				

				
				TblDetallesCompra detalleAux =new TblDetallesCompra();

				detalleAux.setCantidad(cantidadProducto);
				productoMostrar.setPrecioCompra(BigDecimal.valueOf(precioProducto));
				productoMostrar.setPrecioVenta1(BigDecimal.valueOf(precio1));
				productoMostrar.setPrecioVenta2(BigDecimal.valueOf(precio2));
				productoMostrar.setPrecioVenta3(BigDecimal.valueOf(precio3));
				detalleAux.setTblProducto(productoMostrar);
				detalleAux.setPrecioCosto(BigDecimal.valueOf(precioProducto));
				BigDecimal subtotal = BigDecimal.valueOf(precioProducto*cantidadProducto);
				detalleAux.setSubtotal(subtotal);
				detalleAux.setPrecioVenta(BigDecimal.valueOf(0));
				detalleAux.setTblCompra(compraCab);
				idAuxListaProducto+=1;
				detalleAux.setIdDetallesCompras(idAuxListaProducto);

				
				listaDetallesCompra.add(detalleAux);
				subtotalListaDetalle = managerCompras.subtotalListaDetallesCompras(listaDetallesCompra);
				ivaListaDetalle = subtotalListaDetalle* 0.12;
				totalListaDetalle = subtotalListaDetalle+ivaListaDetalle;
				
			} else {
				System.out.println("Veridificando si entra cuando NO es vacio");
				
				for (int i = 0; i < listaDetallesCompra.size(); i++) {
					System.out.println("Imprimiendo la descripcion: "+ listaDetallesCompra.get(i).getIdDetallesCompras()+
							"y " +listaDetallesCompra.get(i).getTblProducto().getDescripcion() );
				}
				
				if (managerCompras.verificarExistencia(listaDetallesCompra, productoMostrar)) {
					System.out.println("Ya existe el producto.");
					JSFUtil.crearMensajeINFO("Producto ya agregado");
				} else {
					System.out.println("Veridificando si entra cuando no se repite");
					compraCab = new TblCompra();
					
					idAuxListaProducto = listaDetallesCompra.size();
					TblDetallesCompra detalleAux =new TblDetallesCompra();

					detalleAux.setCantidad(cantidadProducto);
					productoMostrar.setPrecioCompra(BigDecimal.valueOf(precioProducto));
					productoMostrar.setPrecioVenta1(BigDecimal.valueOf(precio1));
					productoMostrar.setPrecioVenta2(BigDecimal.valueOf(precio2));
					productoMostrar.setPrecioVenta3(BigDecimal.valueOf(precio3));
					detalleAux.setTblProducto(productoMostrar);
					detalleAux.setPrecioCosto(BigDecimal.valueOf(precioProducto));
					BigDecimal subtotal = BigDecimal.valueOf(precioProducto*cantidadProducto);
					detalleAux.setSubtotal(subtotal);
					//campo que est� demas, valor cero para evitar errores.
					detalleAux.setPrecioVenta(BigDecimal.valueOf(0));
					detalleAux.setTblCompra(compraCab);
					idAuxListaProducto+=1;
					detalleAux.setIdDetallesCompras(idAuxListaProducto);

					
					listaDetallesCompra.add(detalleAux);
					subtotalListaDetalle = managerCompras.subtotalListaDetallesCompras(listaDetallesCompra);
					ivaListaDetalle = subtotalListaDetalle* 0.12;
					totalListaDetalle = subtotalListaDetalle+ivaListaDetalle;
					
				}
				
			}
		}else {
			System.out.println("Datos se vuelven cero stock y cantidad");
			JSFUtil.crearMensajeWARN("Elija al menos un proveedor y producto");
			nuevoStock =0;
			cantidadProducto=0;
		}
		
	}
	
	public void actionListenerGuardarCompra() {
		try {
			if (listaDetallesCompra.isEmpty() || proveedorMostrar !=null) {
				JSFUtil.crearMensajeWARN("Verifique:\nLista de detalles vac�a\n o sin proveedor");
			}else {
				
				System.out.println("mostrando el proveedor al guardar compra"+ proveedorMostrar);
				//limpiamos el compo id ya que es incremental automatico en la bdd
				limpiarIdListaAuxiliar();
				managerCompras.guardarCompra(listaDetallesCompra, proveedorMostrar);
				listaDetallesCompra.clear();
				
				subtotalListaDetalle = 0;
				ivaListaDetalle = subtotalListaDetalle* 0.12;
				totalListaDetalle = subtotalListaDetalle+ivaListaDetalle;
				JSFUtil.crearMensajeINFO("Compra guardada exitosamente");
				//PrimeFaces.current().resetInputs("formCabeceraCompra:panelCabeceraCompra");
				listaNuevoPrecios.clear();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	public void limpiarIdListaAuxiliar() {
		for (int i = 0; i < listaDetallesCompra.size(); i++) {
			listaDetallesCompra.get(i).setIdDetallesCompras(null);
		}
	}
	public void actionListenerActualizarDetalleCompra(){
		managerCompras.actualizarDetalleTemporal(listaDetallesCompra, detalleCompraSeleccionado);
		//Datos del footer de la compras
		subtotalListaDetalle = managerCompras.subtotalListaDetallesCompras(listaDetallesCompra);
		ivaListaDetalle = subtotalListaDetalle* 0.12;
		totalListaDetalle = subtotalListaDetalle+ivaListaDetalle;
		
	}
	public void actionListenerSeleccionarDetalleCompra(TblDetallesCompra detalleSeleccionado) {
		this.detalleCompraSeleccionado = detalleSeleccionado;
		System.out.println("mirando el valor del detalle seleccionado a editar: "+ detalleCompraSeleccionado.getTblProducto().getDescripcion());
	}
	public void actionListenerEliminarDetalleListaTemporal(TblDetallesCompra detalleEliminar) {
		try {
			managerCompras.borrarDetalleListaTemporal(listaDetallesCompra, detalleEliminar);
			//Datos del footer de la compras
					subtotalListaDetalle = managerCompras.subtotalListaDetallesCompras(listaDetallesCompra);
					ivaListaDetalle = subtotalListaDetalle* 0.12;
					totalListaDetalle = subtotalListaDetalle+ivaListaDetalle;
			
		} catch (Exception e) {
			System.out.println(e);
		}
		
	}
	
	public void actionListenerCalcularPrecios() {
		try {

			precio1 = (precioProducto *(porcentaje1/100))+ precioProducto;
			precio2 = (precioProducto *(porcentaje2/100))+ precioProducto;
			precio3 = (precioProducto *(porcentaje3/100))+ precioProducto;
			
			ganancia1 = precio1- precioProducto;
			ganancia2 = precio2- precioProducto;
			ganancia3 = precio3- precioProducto;
			
	
			listaNuevoPrecios.clear();
			
			listaNuevoPrecios = managerCompras.calcularPreciosProducto(precioProducto,porcentaje1 , porcentaje2, porcentaje3);
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	public void mostrarPorcentaje() {
		System.out.println(productoMostrar.getPrecioCompra());
	}
	
	public void findAllCompras() {

		listaCompras = managerCompras.findAllCompras();

	}
	public void findAllDetallesComprasByIdCompra(TblCompra compra) {
		listaDetallesByIdCompra = managerCompras.ObtenerDetalleCompraByIdCompra(compra.getIdCompra());
	}

	
	public String actionReporteCompras(TblCompra compra) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		int compraSeleccionada = compra.getIdCompra();
		parametros.put("idCompra", compraSeleccionada);
		FacesContext context = FacesContext.getCurrentInstance();
		ServletContext servletContext = (ServletContext) context.getExternalContext().getContext();
		String ruta = servletContext.getRealPath("compras/reporte_compras.jasper");
		System.out.println(ruta);
		HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
		response.addHeader("Content-disposition", "attachment;filename=reporte_compras.pdf");
		response.setContentType("application/pdf");
		System.out.println("Reporte factura");
		try {
			Class.forName("org.postgresql.Driver");
			Connection connection = null;
			connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/easytech", "postgres", "root");
			JasperPrint impresion = JasperFillManager.fillReport(ruta, parametros, connection);
			JasperExportManager.exportReportToPdfStream(impresion, response.getOutputStream());
			context.getApplication().getStateManager().saveView(context);
			System.out.println("reporte generado.");
			context.responseComplete();

		} catch (Exception e) {
			JSFUtil.crearMensajeERROR(e.getMessage());
			e.printStackTrace();

		}
		return "";

	}
	

	/*
	 * getters and setters
	 */
	public TblPersona getPersona() {
		return persona;
	}

	public void setPersona(TblPersona persona) {
		this.persona = persona;
	}

	public List<TblPersona> getListPersonas() {
		return listPersonas;
	}

	public void setListPersonas(List<TblPersona> listPersonas) {
		this.listPersonas = listPersonas;
	}

	public TblProveedores getProveedor() {
		return proveedor;
	}

	public void setProveedor(TblProveedores proveedor) {
		this.proveedor = proveedor;
	}

	public List<TblProveedores> getListProveedores() {
		return listProveedores;
	}

	public void setListProveedores(List<TblProveedores> listProveedores) {
		this.listProveedores = listProveedores;
	}

	public String getNombreComercial() {
		return nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public TblProveedores getProveedorSeleccionado() {
		return proveedorSeleccionado;
	}

	public void setProveedorSeleccionado(TblProveedores proveedorSeleccionado) {
		this.proveedorSeleccionado = proveedorSeleccionado;
	}

	public TblCategoria getCategoria() {
		return categoria;
	}

	public void setCategoria(TblCategoria categoria) {
		this.categoria = categoria;
	}

	public List<TblCategoria> getListCategoria() {
		return listCategoria;
	}

	public void setListCategoria(List<TblCategoria> listCategoria) {
		this.listCategoria = listCategoria;
	}

	public String getNombreCategoria() {
		return nombreCategoria;
	}

	public void setNombreCategoria(String nombreCategoria) {
		this.nombreCategoria = nombreCategoria;
	}

	public TblCategoria getCategoriaSeleccionado() {
		return categoriaSeleccionado;
	}

	public void setCategoriaSeleccionado(TblCategoria categoriaSeleccionado) {
		this.categoriaSeleccionado = categoriaSeleccionado;
	}

	public int getIdSubcategoria() {
		return idSubcategoria;
	}

	public void setIdSubcategoria(int idSubcategoria) {
		this.idSubcategoria = idSubcategoria;
	}

	public List<TblCategoria> getListaSub_subCategorias() {
		return listaSub_subCategorias;
	}

	public void setListaSub_subCategorias(List<TblCategoria> listaSub_subCategorias) {
		this.listaSub_subCategorias = listaSub_subCategorias;
	}

	public List<SelectItem> getListasSelectItemSubcategorias() {
		return listasSelectItemSubcategorias;
	}

	public void setListasSelectItemSubcategorias(List<SelectItem> listasSelectItemSubcategorias) {
		this.listasSelectItemSubcategorias = listasSelectItemSubcategorias;
	}

	public int getIdCategoriaPrincipal() {
		return idCategoriaPrincipal;
	}

	public void setIdCategoriaPrincipal(int idCategoriaPrincipal) {
		this.idCategoriaPrincipal = idCategoriaPrincipal;
	}

	public List<TblCategoria> getListaCategoriasB() {
		return listaCategoriasB;
	}

	public void setListaCategoriasB(List<TblCategoria> listaCategoriasB) {
		this.listaCategoriasB = listaCategoriasB;
	}

	public List<TblProducto> getListProducto() {
		return listProducto;
	}

	public void setListProducto(List<TblProducto> listProducto) {
		this.listProducto = listProducto;
	}

	public TblProveedores getProveedorMostrar() {
		return proveedorMostrar;
	}

	public void setProveedorMostrar(TblProveedores proveedorMostrar) {
		this.proveedorMostrar = proveedorMostrar;
	}

	public TblProducto getProductoMostrar() {
		return productoMostrar;
	}

	public void setProductoMostrar(TblProducto productoMostrar) {
		this.productoMostrar = productoMostrar;
	}

	public int getIdProveedorMostrar() {
		return idProveedorMostrar;
	}

	public void setIdProveedorMostrar(int idProveedorMostrar) {
		this.idProveedorMostrar = idProveedorMostrar;
	}

	public int getIdProductoMostrar() {
		return idProductoMostrar;
	}

	public void setIdProductoMostrar(int idProductoMostrar) {
		this.idProductoMostrar = idProductoMostrar;
	}

	public int getCantidadProducto() {
		return cantidadProducto;
	}

	public void setCantidadProducto(int cantidadProducto) {
		this.cantidadProducto = cantidadProducto;
	}

	public double getnuevoStock() {
		return nuevoStock;
	}

	public void setnuevoStock(double stock) {
		this.nuevoStock = stock;
	}

	public double getPrecioProducto() {
		return precioProducto;
	}

	public void setPrecioProducto(double precioProducto) {
		this.precioProducto = precioProducto;
	}

	public List<Double> getListaNuevoPrecios() {
		return listaNuevoPrecios;
	}

	public void setListaNuevoPrecios(List<Double> listaNuevoPrecios) {
		this.listaNuevoPrecios = listaNuevoPrecios;
	}

	public List<TblDetallesCompra> getListaDetallesCompra() {
		return listaDetallesCompra;
	}

	public void setListaDetallesCompra(List<TblDetallesCompra> listaDetallesCompra) {
		this.listaDetallesCompra = listaDetallesCompra;
	}

	public int getIdAuxListaProducto() {
		return idAuxListaProducto;
	}

	public void setIdAuxListaProducto(int idAuxListaProducto) {
		this.idAuxListaProducto = idAuxListaProducto;
	}

	public double getSubtotalListaDetalle() {
		return subtotalListaDetalle;
	}

	public void setSubtotalListaDetalle(double subtotalListaDetalle) {
		this.subtotalListaDetalle = subtotalListaDetalle;
	}

	public double getTotalListaDetalle() {
		return totalListaDetalle;
	}

	public void setTotalListaDetalle(double totalListaDetalle) {
		this.totalListaDetalle = totalListaDetalle;
	}

	public double getIvaListaDetalle() {
		return ivaListaDetalle;
	}

	public void setIvaListaDetalle(double ivaListaDetalle) {
		this.ivaListaDetalle = ivaListaDetalle;
	}

	public TblDetallesCompra getDetalleCompraSeleccionado() {
		return detalleCompraSeleccionado;
	}

	public void setDetalleCompraSeleccionado(TblDetallesCompra detalleCompraSeleccionado) {
		this.detalleCompraSeleccionado = detalleCompraSeleccionado;
	}

	public double getPrecio1() {
		return precio1;
	}

	public void setPrecio1(double precio1) {
		this.precio1 = precio1;
	}

	public double getPrecio2() {
		return precio2;
	}

	public void setPrecio2(double precio2) {
		this.precio2 = precio2;
	}

	public double getPrecio3() {
		return precio3;
	}

	public void setPrecio3(double precio3) {
		this.precio3 = precio3;
	}

	public double getPorcentaje1() {
		return porcentaje1;
	}

	public void setPorcentaje1(double porcentaje1) {
		this.porcentaje1 = porcentaje1;
	}

	public double getPorcentaje2() {
		return porcentaje2;
	}

	public void setPorcentaje2(double porcentaje2) {
		this.porcentaje2 = porcentaje2;
	}

	public double getPorcentaje3() {
		return porcentaje3;
	}

	public void setPorcentaje3(double porcentaje3) {
		this.porcentaje3 = porcentaje3;
	}

	public double getGanancia1() {
		return ganancia1;
	}

	public void setGanancia1(double ganancia1) {
		this.ganancia1 = ganancia1;
	}

	public double getGanancia2() {
		return ganancia2;
	}

	public void setGanancia2(double ganancia2) {
		this.ganancia2 = ganancia2;
	}

	public double getGanancia3() {
		return ganancia3;
	}

	public void setGanancia3(double ganancia3) {
		this.ganancia3 = ganancia3;
	}

	public List<TblCompra> getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(List<TblCompra> listaCompras) {
		this.listaCompras = listaCompras;
	}

	public List<TblDetallesCompra> getListaDetallesByIdCompra() {
		return listaDetallesByIdCompra;
	}

	public void setListaDetallesByIdCompra(List<TblDetallesCompra> listaDetallesByIdCompra) {
		this.listaDetallesByIdCompra = listaDetallesByIdCompra;
	}

	public boolean isEsCategoriaPricipal() {
		return esCategoriaPricipal;
	}

	public void setEsCategoriaPricipal(boolean esCategoriaPricipal) {
		this.esCategoriaPricipal = esCategoriaPricipal;
	}

	public String getDisplayListaSubcategoria() {
		return displayListaSubcategoria;
	}

	public void setDisplayListaSubcategoria(String displayListaSubcategoria) {
		this.displayListaSubcategoria = displayListaSubcategoria;
	}

	public List<TblCategoria> getListaSeparada() {
		return listaSeparada;
	}

	public void setListaSeparada(List<TblCategoria> listaSeparada) {
		this.listaSeparada = listaSeparada;
	}

	public boolean isEsCategoriaPricipalEdicion() {
		return esCategoriaPricipalEdicion;
	}

	public void setEsCategoriaPricipalEdicion(boolean esCategoriaPricipalEdicion) {
		this.esCategoriaPricipalEdicion = esCategoriaPricipalEdicion;
	}

	public int getIdCategoriaPrincipalEdicion() {
		return idCategoriaPrincipalEdicion;
	}

	public void setIdCategoriaPrincipalEdicion(int idCategoriaPrincipalEdicion) {
		this.idCategoriaPrincipalEdicion = idCategoriaPrincipalEdicion;
	}

	public String getDisplayListaSubcategoriaEdicion() {
		return displayListaSubcategoriaEdicion;
	}

	public void setDisplayListaSubcategoriaEdicion(String displayListaSubcategoriaEdicion) {
		this.displayListaSubcategoriaEdicion = displayListaSubcategoriaEdicion;
	}
	
	
	
	
}
