package easytech.model.cobranzas.dtos;

public class DTOpersona {
  private String cedula;
  private String nombres;
  private String apellidos;
  private String direccion;
  private String telefono;
  private String email;

  public DTOpersona() {
    super();
    this.cedula = "";
    this.nombres = "";
    this.apellidos = "";
    this.direccion = "";
    this.telefono = "";
    this.email = "";
  }

  public DTOpersona(String cedula, String nombres, String apellidos, String direccion, String telefono,
      String email) {
    super();
    this.cedula = cedula;
    this.nombres = nombres;
    this.apellidos = apellidos;
    this.direccion = direccion;
    this.telefono = telefono;
    this.email = email;
  }

  public String getCedula() {
    return cedula;
  }

  public void setCedula(String cedula) {
    this.cedula = cedula;
  }

  public String getNombres() {
    return nombres;
  }

  public void setNombres(String nombres) {
    this.nombres = nombres;
  }

  public String getApellidos() {
    return apellidos;
  }

  public void setApellidos(String aplelidos) {
    this.apellidos = aplelidos;
  }

  public String getDireccion() {
    return direccion;
  }

  public void setDireccion(String direccion) {
    this.direccion = direccion;
  }

  public String getTelefono() {
    return telefono;
  }

  public void setTelefono(String telefono) {
    this.telefono = telefono;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

}