package easytech.model.cobranzas.managers;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Period;
import java.time.chrono.ThaiBuddhistChronology;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.xml.bind.ParseConversionEvent;

import easytech.model.cobranzas.dtos.DTOpersona;
import minimarketdemo.model.core.entities.TblAbono;
import minimarketdemo.model.core.entities.TblAcuerdo;
import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblCuota;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.entities.TblTasaNominal;
import minimarketdemo.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerCobranzas
 */
@Stateless
@LocalBean
public class ManagerCobranzas {

	@PersistenceContext
	private EntityManager em;

	@EJB
	private ManagerDAO mDAO;

	public ManagerCobranzas() {

	}

	// guardar tasa nomimnal

	public List<TblCliente> findAllCliente() {
		return mDAO.findWhere(TblCliente.class, "o.estado=" + "true", null);
	}

	public TblAcuerdo ObtenerDatosAcuerdo(int idAcuerdo) {
		TblAcuerdo tblAcuerdo = null;
		String clausula = "idAcuerdo = '" + idAcuerdo + "'";
		try {
			tblAcuerdo = (TblAcuerdo) mDAO.findWhereObject(TblAcuerdo.class, clausula);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return tblAcuerdo;
	}

	public void actualizarCuota(TblCuota tblCuota) {
		try {
			TblCuota tblMCuota;
			String clausula = "idCuota = '" + tblCuota.getIdCuota() + "'";
			tblMCuota = (TblCuota) mDAO.findWhereObject(TblCuota.class, clausula);
			tblMCuota.setCuota(new BigDecimal(0));
			em.merge(tblMCuota);
			System.out.println("actualizado");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public List<TblCuota> ObtenerCuotasAcuerdo(int idAcuerdo) {

		List<TblCuota> listaCuotas = null;
		String clausulaListaCuotas = "id_acuerdo = '" + idAcuerdo + "'";

		TblCuota tblCuota;
		try {
			listaCuotas = (List<TblCuota>) mDAO.findWhere(TblCuota.class, clausulaListaCuotas, null);
//			tblCuota = (TblCuota) mDAO.findWhereObject(TblCuota.class, clausulaListaCuotas);
			DateFormat format = new SimpleDateFormat("dd");
			Date fecha = new Date();
			String formatoMesFechaActual = format.format(fecha).toString();

			for (int i = 0; i < listaCuotas.size(); i++) {

				if (fecha.getMonth() == listaCuotas.get(i).getFechaPago().getMonth()) {

					String clausulaCuota = "idCuota = '" + listaCuotas.get(i).getIdCuota() + "'";
					tblCuota = (TblCuota) mDAO.findWhereObject(TblCuota.class, clausulaCuota);
					BigDecimal valor = new BigDecimal(0.00);
					System.out.println("Valor cuotas: " + listaCuotas.get(i).getCuota());
					System.out.println("Valor: " + valor);
					int valorx = Integer.valueOf(listaCuotas.get(i).getCuota().intValue());
					System.out.println(valorx);
					if (valorx == 0) {
						tblCuota.setDiasVencidos(0);
						tblCuota.setDiasXVencer(0);
						em.merge(tblCuota);
					} else {
						Date fechacuota = listaCuotas.get(i).getFechaPago();

						String formatoMesFechaCuota = format.format(fechacuota).toString();
						int mesCuota = Integer.parseInt(formatoMesFechaCuota);
						int mesActual = Integer.parseInt(formatoMesFechaActual);
						int diasCalculo = mesCuota - mesActual;
						System.out.println("Dias calculados: " + diasCalculo);
						if (diasCalculo < 0) {
							diasCalculo = diasCalculo * -1;
							tblCuota.setDiasVencidos(diasCalculo);
						} else {
							tblCuota.setDiasXVencer(diasCalculo);
						}

						em.merge(tblCuota);
					}

				} else {
					break;
				}

			}
			listaCuotas = (List<TblCuota>) mDAO.findWhere(TblCuota.class, clausulaListaCuotas, null);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return listaCuotas;
	}

	public List<TblCliente> findAllAcuerdoClientes() {
		List<TblAcuerdo> listasAcuerdos;
		List<TblCliente> listasClientes;

		TblCliente tblCliente = null;
		List<TblCliente> listaResultClientes = null;
		listasClientes = mDAO.findAll(TblCliente.class);
		listasAcuerdos = mDAO.findAll(TblAcuerdo.class);
		try {

			for (int i = 0; i < listasClientes.size(); i++) {
				int idCliente = listasClientes.get(i).getIdCliente();
				System.out.println("IDCLienteProbando: " + idCliente);
				for (int j = 0; j < listasAcuerdos.size(); j++) {
					if (idCliente == listasAcuerdos.get(i).getTblCliente().getIdCliente()) {
						System.out.println(
								"TrueProbando: " + (idCliente == listasAcuerdos.get(i).getTblCliente().getIdCliente()));
						tblCliente.setTblPersona(listasAcuerdos.get(i).getTblCliente().getTblPersona());
						tblCliente.setIdCliente(listasAcuerdos.get(i).getTblCliente().getIdCliente());
						listasClientes.add(tblCliente);
						break;
					}
				}

			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return listasClientes;
	}

	public List<TblAcuerdo> findAllAcuerdosClientes() {
		Query q = em.createQuery("SELECT o FROM TblAcuerdo o", TblAcuerdo.class);
		return q.getResultList();
	}

	public List<TblTasaNominal> findAllNominal() {
		Query q = em.createQuery("SELECT o FROM TblTasaNominal o", TblTasaNominal.class);

		return q.getResultList();

	}

	public void realizarAbonoCuota(TblCuota tblCuota, double abono) {
		try {
			TblAcuerdo tblAcuerdo = new TblAcuerdo();
			String clausulaAcuerdo = "idAcuerdo = '" + tblCuota.getTblAcuerdo().getIdAcuerdo() + "'";
			tblAcuerdo = (TblAcuerdo) mDAO.findWhereObject(TblAcuerdo.class, clausulaAcuerdo);

			TblAbono tblAbono = new TblAbono();
			tblAbono.setAbono(new BigDecimal(abono));
			tblAbono.setTblCuota(tblCuota);
			tblAbono.setFechaAbono(new Date());

			BigDecimal abonoBD = new BigDecimal(abono);
			BigDecimal cuoataPagarBD = tblCuota.getCuota();
			BigDecimal saldo = cuoataPagarBD.subtract(abonoBD);
			tblAbono.setSaldo(saldo);
			em.persist(tblAbono);

			TblCuota tblMCuota;
			String clausula = "idCuota = '" + tblCuota.getIdCuota() + "'";
			tblMCuota = (TblCuota) mDAO.findWhereObject(TblCuota.class, clausula);
			tblMCuota.setCuota(saldo);
			em.merge(tblMCuota);

			tblAcuerdo.setTotal(tblAcuerdo.getTotal().subtract(abonoBD));
			em.merge(tblAcuerdo);
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public DTOpersona findClientByCedula(int idCliente) {
		DTOpersona dtoPersona = new DTOpersona();
		TblPersona tblPersona = new TblPersona();
		TblCliente tblCliente = new TblCliente();
		String clausula = "idCliente = '" + idCliente + "'";
		String clausulaCliente;
		try {

			tblCliente = (TblCliente) mDAO.findWhereObject(TblCliente.class, clausula);
			int id = tblCliente.getTblPersona().getIdPersona();
			clausulaCliente = "idPersona='" + id + "'";
			tblPersona = (TblPersona) mDAO.findWhereObject(TblPersona.class, clausulaCliente);
			dtoPersona.setCedula(tblPersona.getCiRuc());
			dtoPersona.setNombres(tblPersona.getNombres());
			dtoPersona.setApellidos(tblPersona.getApellidos());
			dtoPersona.setTelefono(tblPersona.getTelefono());
			dtoPersona.setDireccion(tblPersona.getDireccion());
			dtoPersona.setEmail(tblPersona.getEmail());

		} catch (Exception e) {

		}
		return dtoPersona;
	}

	public List<TblAcuerdo> getIdsAcuerdosByCliente(int idCliente) {
		List<TblAcuerdo> tblAcuerdo = new ArrayList<TblAcuerdo>();
		try {

			String clausula = "id_cliente = '" + idCliente + "'";
			tblAcuerdo = (List<TblAcuerdo>) mDAO.findWhere(TblAcuerdo.class, clausula, null);
		} catch (Exception e) {
			// TODO: handle exception
		}

		return tblAcuerdo;
	}

	public void GenerarCuotasCreditoContrato(TblCliente tblCliente, double tasa__nominal, double monto, int tiempo) {
		double base;
		double exponenteA, exponenteB;
		double tasa_efectiva;
		double interes;
		double total;
		double cuotas;
		base = (1 + (tasa__nominal / 100));
		exponenteA = (tiempo * 30);
		exponenteB = exponenteA / 360;
		tasa_efectiva = (Math.pow(base, exponenteB) - 1) * 100;
		interes = monto * (tasa_efectiva / 100);
		total = monto + interes;
		cuotas = total / tiempo;
		TblAcuerdo tblAcuerdo = new TblAcuerdo();
		tblAcuerdo.setMonto(new BigDecimal(monto));
		tblAcuerdo.setEstado(true);
		tblAcuerdo.setPeriodo(tiempo);
		tblAcuerdo.setTasaEfectiva(new BigDecimal(tasa_efectiva));
		tblAcuerdo.setTblCliente(tblCliente);
		TblTasaNominal tblTasaNominal = new TblTasaNominal();
		tblTasaNominal.setIdTasaNominal(1);
		tblAcuerdo.setTblTasaNominal(tblTasaNominal);
		tblAcuerdo.setValorPorInteres(new BigDecimal(interes));
		tblAcuerdo.setTotal(new BigDecimal(total));
		Date f = new Date();
		tblAcuerdo.setFechaPrimerPago(f);
		em.persist(tblAcuerdo);
		f.setMonth(f.getMonth() + 1);

		TblCuota tblCuota = new TblCuota();
		for (int i = 0; i < tiempo; i++) {
			tblCuota = new TblCuota();
			tblCuota.setTblAcuerdo(tblAcuerdo);
			tblCuota.setEstado(true);

			tblCuota.setCuota(new BigDecimal(cuotas));
			tblCuota.setFechaPago(f);
			f = new Date(f.getYear(), (f.getMonth() + 1), f.getDate());
			em.persist(tblCuota);
			System.out.println("Exito: " + i);
		}

	}

}