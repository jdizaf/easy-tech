package easytech.model.productos.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.New;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import easytech.model.productos.dtos.DTOproductos;

import minimarketdemo.model.core.entities.TblCategoria;
import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.entities.TblProducto;
import minimarketdemo.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerOrdenTrabajo
 */
@Stateless
@LocalBean
public class ManagerProductos {

	@PersistenceContext
	private EntityManager em;
	@EJB
	private ManagerDAO mDAO;

	// VAriables Extras
	private TblProducto tblProducto;
	private List<TblProducto> listaProductosAux;

	private List<TblProducto> listaProductosAux2;

	public ManagerProductos() {
		// TODO Auto-generated constructor stub
	}

	public List<TblProducto> findAllProductos() {
		try {
			System.out.println("Probando 1");
			tblProducto = new TblProducto();
			listaProductosAux2 = new ArrayList<TblProducto>();
			listaProductosAux = em.createNamedQuery("TblProducto.findAll", TblProducto.class).getResultList();
//
//			System.out.println("Probando ");
//			for (int i = 0; i < listaProductosAux.size(); i++) {
//				System.out.println("a: " + listaProductosAux.size());
//				String descripcion = listaProductosAux.get(i).getDescripcion();
//				System.out.println("descripcion: " + descripcion);
//				System.out.println("B");
//				String[] parts = descripcion.split(", ");
//				System.out.println("C");
//				System.out.println("Tamaño: " + parts.length);
//				System.out.println("sdfsdfsd fsd f sd f sd fs df sd f sdf sd");
//				if (parts.length == 0) {
//					tblProducto = listaProductosAux.get(i);
//
//				} else {
//					tblProducto = listaProductosAux.get(i);
//					tblProducto.setDescripcion(parts[0]);
//
//				}
//				listaProductosAux2.add(tblProducto);
//			}

		} catch (Exception e) {
			// TODO: handle exception
		}
		return listaProductosAux;

	}

	public List<TblCategoria> findCategoriaPrincipal() {
		List<TblCategoria> tblCategoria = null;
		String clausula = "tbl_id_categoria is null";

		try {

			tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class, clausula, null);
			System.out.println(tblCategoria);

		} catch (Exception e) {

		}
		return tblCategoria;
	}

	public void registrarProducto(DTOproductos dtoProductos) {
		// insertar Producto
		TblProducto tblProducto = new TblProducto();
		System.out.println("1");
		TblCategoria tblCategoria = new TblCategoria();
		System.out.println("2");
		tblCategoria.setIdCategoria(dtoProductos.getIdCategoria());
		System.out.println("3");
		tblProducto.setTblCategoria(tblCategoria);
		System.out.println("4");
		if (dtoProductos.getModelo().equals("") & dtoProductos.getSerie().equals("")) {
			System.out.println("51");
			tblProducto.setDescripcion(dtoProductos.getDescripcion_nombre());
		} else {
			String concatDescripcion = dtoProductos.getDescripcion_nombre() + ", " + dtoProductos.getMarca() + ", "
					+ dtoProductos.getModelo() + ", " + dtoProductos.getSerie();
			tblProducto.setDescripcion(concatDescripcion);
			System.out.println("52");
		}
		tblProducto.setEstado(dtoProductos.isEstado());
		tblProducto.setImagen(null);
		tblProducto.setPrecioCompra(new BigDecimal(dtoProductos.getPrecio_compra()));
		tblProducto.setPrecioVenta1(new BigDecimal(dtoProductos.getPrecio_venta1()));
		tblProducto.setPrecioVenta2(new BigDecimal(dtoProductos.getPrecio_venta2()));
		tblProducto.setPrecioVenta3(new BigDecimal(dtoProductos.getPrecio_venta3()));
		tblProducto.setImagen(dtoProductos.getImagen());
		System.out.println("6");
		tblProducto.setStock(dtoProductos.getStock());

		em.persist(tblProducto);

	}

	public List<TblCategoria> findSubCategorias(int idCategoriaPrincipal) {
		List<TblCategoria> tblCategoria = null;
		String clausula = "tbl_id_categoria ='" + idCategoriaPrincipal + "'";

		try {

			tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class, clausula, null);
			System.out.println(tblCategoria);

		} catch (Exception e) {

		}
		return tblCategoria;
	}

	public TblProducto mostrarProducto(int idProducto) {
		TblProducto tblproducto = new TblProducto();
		String clausula = "idProducto ='" + idProducto + "'";

		try {
			tblproducto = (TblProducto) mDAO.findWhereObject(TblProducto.class, clausula);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return tblproducto;
	}

	public List<Double> calcularPreciosProducto(Double precio, double porcentaje1, double porcentaje2,
			double porcentaje3) {
		List<Double> listaPrecios = new ArrayList<Double>();

		Double precio1 = precio * (porcentaje1 / 100) + precio;
		Double precio2 = precio * (porcentaje2 / 100) + precio;
		Double precio3 = precio * (porcentaje3 / 100) + precio;

		listaPrecios.add(precio1);
		listaPrecios.add(precio2);
		listaPrecios.add(precio3);

		return listaPrecios;

	}

	public void EliminarProducto(int idProducto) {
		try {
			System.out.println("aaaaa_ " + idProducto);
			TblProducto tblProducto = (TblProducto) mDAO.findById(TblProducto.class, idProducto);
			mDAO.eliminar(TblProducto.class, tblProducto);
//			JSFUtil.crearMensajeERROR("Eliminado");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
//			JSFUtil.crearMensajeERROR("Error: "+e);

		}

	}

	public void ActualizarProducto(TblProducto tblProducto) {
		TblProducto tblProductoE = new TblProducto();
		String clausula = "idProducto ='" + tblProducto.getIdProducto() + "'";
		try {
			tblProductoE = (TblProducto) mDAO.findWhereObject(TblProducto.class, clausula);
			tblProductoE.setDescripcion(tblProducto.getDescripcion());
			tblProductoE.setEstado(tblProducto.getEstado());
			tblProductoE.setImagen(tblProducto.getImagen());
			tblProductoE.setPrecioCompra(tblProducto.getPrecioCompra());
			tblProductoE.setPrecioVenta1(tblProducto.getPrecioVenta1());
			tblProductoE.setPrecioVenta2(tblProducto.getPrecioVenta2());
			tblProductoE.setPrecioVenta3(tblProducto.getPrecioVenta3());
			tblProductoE.setStock(tblProducto.getStock());
			tblProductoE.setTblCategoria(tblProducto.getTblCategoria());

			mDAO.actualizar(tblProductoE);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}