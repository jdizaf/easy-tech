package easytech.model.productos.dtos;

public class DTOproductos {
	private int idCategoria;
	private String descripcion_nombre;
	private String marca;
	private String modelo;
	private String serie;
	private double precio_compra;
	private double precio_venta1;
	private double precio_venta2;
	private double precio_venta3;
	private byte[] imagen;
	private int stock;
	private boolean estado;

	public DTOproductos(int idCategoria, String descripcion_marca, String modelo, String serie, double precio_compra,
			double precio_venta1, double precio_venta2, double precio_venta3, byte[] imagen, int stock,
			boolean estado) {
		super();
		this.idCategoria = idCategoria;
		this.descripcion_nombre = descripcion_marca;
		this.modelo = modelo;
		this.serie = serie;
		this.precio_compra = precio_compra;
		this.precio_venta1 = precio_venta1;
		this.precio_venta2 = precio_venta2;
		this.precio_venta3 = precio_venta3;
		this.imagen = imagen;
		this.stock = stock;
		this.estado = estado;
	}

	public DTOproductos() {
		super();
		this.idCategoria = 0;

		this.descripcion_nombre = "";
		this.modelo = "";
		this.serie = "";
		this.marca = "";
		this.precio_compra = 0;
		this.precio_venta1 = 0;
		this.precio_venta2 = 0;
		this.precio_venta3 = 0;
		this.imagen = null;
		this.stock = 0;
		this.estado = true;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getDescripcion_nombre() {
		return descripcion_nombre;
	}

	public void setDescripcion_nombre(String descripcion_marca) {
		this.descripcion_nombre = descripcion_marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public double getPrecio_compra() {
		return precio_compra;
	}

	public void setPrecio_compra(double precio_compra) {
		this.precio_compra = precio_compra;
	}

	public double getPrecio_venta1() {
		return precio_venta1;
	}

	public void setPrecio_venta1(double precio_venta1) {
		this.precio_venta1 = precio_venta1;
	}

	public double getPrecio_venta2() {
		return precio_venta2;
	}

	public void setPrecio_venta2(double precio_venta2) {
		this.precio_venta2 = precio_venta2;
	}

	public double getPrecio_venta3() {
		return precio_venta3;
	}

	public void setPrecio_venta3(double precio_venta3) {
		this.precio_venta3 = precio_venta3;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String nombre) {
		this.marca = nombre;
	}

}