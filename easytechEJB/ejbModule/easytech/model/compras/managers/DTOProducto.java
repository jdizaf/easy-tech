package easytech.model.compras.managers;

import java.io.Serializable;
import java.math.BigDecimal;

import minimarketdemo.model.core.entities.TblCategoria;

public class DTOProducto implements Serializable {

	private TblCategoria categoria;
	private String descripcion;
	private BigDecimal precioCompra;
	private BigDecimal precio_venta_1;
	private BigDecimal precio_venta_2;
	private BigDecimal precio_venta_3;
	private byte[] imagen;
	private int stock;
	private boolean estado;
	public TblCategoria getCategoria() {
		return categoria;
	}
	public void setCategoria(TblCategoria id_categoria) {
		this.categoria = id_categoria;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public BigDecimal getPrecioCompra() {
		return precioCompra;
	}
	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}
	public BigDecimal getPrecio_venta_1() {
		return precio_venta_1;
	}
	public void setPrecio_venta_1(BigDecimal precio_venta_1) {
		this.precio_venta_1 = precio_venta_1;
	}
	public BigDecimal getPrecio_venta_2() {
		return precio_venta_2;
	}
	public void setPrecio_venta_2(BigDecimal precio_venta_2) {
		this.precio_venta_2 = precio_venta_2;
	}
	public BigDecimal getPrecio_venta_3() {
		return precio_venta_3;
	}
	public void setPrecio_venta_3(BigDecimal precio_venta_3) {
		this.precio_venta_3 = precio_venta_3;
	}
	public byte[] getImagen() {
		return imagen;
	}
	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	
}
