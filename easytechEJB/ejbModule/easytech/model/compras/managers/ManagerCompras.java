package easytech.model.compras.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sound.midi.Soundbank;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import easytech.model.compras.managers.JSFUtil;
import minimarketdemo.model.core.entities.TblCategoria;
import minimarketdemo.model.core.entities.TblCompra;
import minimarketdemo.model.core.entities.TblDetallesCompra;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.entities.TblProducto;
import minimarketdemo.model.core.entities.TblProveedores;
import minimarketdemo.model.core.managers.ManagerDAO;

/**
 * Session Bean implementation class ManagerCompras
 */
@Stateless
@LocalBean
public class ManagerCompras {

	@PersistenceContext
	private EntityManager em;
	private ManagerDAO mDAO;
	/**
	 * Default constructor.
	 */
	public ManagerCompras() {
		// TODO Auto-generated constructor stub
	}

	
	/*M�todos para la insercion de proveedores
	 */
	public void insertarProveedor(DTOPersona persona, String nombreComercial) {
		try {
			// insertar persona
			TblPersona tblPersona = new TblPersona();
			tblPersona.setCiRuc(persona.getCedula());
			tblPersona.setNombres(persona.getNombres());
			tblPersona.setApellidos(persona.getApellidos());
			tblPersona.setDireccion(persona.getDireccion());
			tblPersona.setTelefono(persona.getTelefono());
			tblPersona.setEmail(persona.getEmail());
			
			System.out.println("metodo insertar proveedor" );
			
			//ingresa solo si no existe en la bdd
			TblPersona cedulaPersona = new TblPersona();
			cedulaPersona = ObtenerPersonasByCedula(tblPersona.getCiRuc().toString());
			System.out.println("viendo la cedula de la persona  "+cedulaPersona);
			if (cedulaPersona == null) {
				System.out.println("validacion de persona insertar si no existe");
				em.persist(tblPersona);
			}
				
		
			
			TblProveedores proveedorAux =  new TblProveedores();
			proveedorAux = ObtenerProveedorByIdPersona(tblPersona.getCiRuc());
			TblProveedores tblproveedor = new TblProveedores();
			System.out.println("probando si encontro al proveedor"+ proveedorAux);
			if (Objects.isNull(proveedorAux) ) {
				System.out.println("validacion de proveedor insertar");
				// insertar Proveedor solo si no existe
				
				tblproveedor.setTblPersona(tblPersona);
				tblproveedor.setEstado(true);
				tblproveedor.setNombreComercial(nombreComercial);
				em.persist(tblproveedor);
				System.out.println("proveedor ingresado");
				JSFUtil.crearMensajeINFO("Proveedor Ingresado.");

			
			}else if(!proveedorAux.getNombreComercial().equals(nombreComercial) ) {
				
				//inserta proveedor solo si no coinciden los nombres comerciales.
				System.out.println("si se repite el nombre "+ proveedorAux.getNombreComercial()+"----el otoro nombre comercial"+nombreComercial);
				// insertar Proveedor
				//TblProveedores tblproveedor = new TblProveedores();
				tblproveedor.setTblPersona(tblPersona);
				tblproveedor.setEstado(true);
				tblproveedor.setNombreComercial(nombreComercial);
				em.persist(tblproveedor);
				JSFUtil.crearMensajeINFO("Persona existente, Nuevo proveedor.");
				System.out.println("proveedor ingresado");
				
			}else {
				JSFUtil.crearMensajeINFO("Proveedor ya existe .");
				
			}

			

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public void actualizarProveedorById(TblProveedores proveedor) {
		try {

			TblProveedores p = findProveedorById(proveedor.getIdProveedor());
			System.out.println("entrando al metodo manager actualizar");
			p.setEstado(proveedor.getEstado());
			p.setTblPersona(proveedor.getTblPersona());
			p.setNombreComercial(proveedor.getNombreComercial());

			System.out.println(p.getTblPersona().getCiRuc());
			System.out.println(p.getTblPersona().getDireccion());
			System.out.println(p.getTblPersona().getNombres());
			System.out.println(p.getTblPersona().getApellidos());
			System.out.println(p.getTblPersona().getEmail());

			em.merge(p);
			System.out.println("actualizado");
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public TblProveedores findProveedorById(int id) {
		return em.find(TblProveedores.class, id);
	}


	public TblPersona ObtenerPersonasByCedula(String cedula) {

		String consulta = "select o from TblPersona o where o.ciRuc = :cedula";
		Query q = em.createQuery(consulta, TblPersona.class).setParameter("cedula", cedula);
		q.setMaxResults(1);

		List<TblPersona> persona = q.getResultList();
		if (persona == null || persona.isEmpty()) {
			//devueleve un objeto vacio.
			return null;
		}
		return persona.get(0);
		// Fuente: https://www.iteramos.com/pregunta/29865/jpa-getsingleresult-o-null
	}

	public TblProveedores ObtenerProveedorByIdPersona(String cedula) {
		
		TblPersona persona = ObtenerPersonasByCedula(cedula);
		int idPersona = persona.getIdPersona();
		
		String consulta = "select o from TblProveedores o JOIN o.tblPersona p where p.idPersona = :idPersona";
		Query q = em.createQuery(consulta, TblProveedores.class).setParameter("idPersona", idPersona);
		q.setMaxResults(1);

		List<TblProveedores> proveedor = q.getResultList();
		//System.out.println("probando el otro metodo");
		if (proveedor == null || proveedor.isEmpty()) {
			return null;
		}
		//System.out.println("Haber " + proveedor.get(0).getNombreComercial());
		return proveedor.get(0);

	}

	public List<TblProveedores> findAllProveedores() {
		System.out.println("entrando al metodo del manager findAllProdveedores");
		TypedQuery<TblProveedores> q = em.createQuery("SELECT c FROM TblProveedores c", TblProveedores.class);
		return q.getResultList();
	}

	
	
	public void eliminarProveedor(int id) {
		System.out.println("entrando al manager");
		TblProveedores proveedor = findProveedorById(id);
		System.out.println(proveedor.getNombreComercial());
		em.remove(proveedor);
	}
	
	/*M�todos para la secci�n de categor�a*/
	
	public void insertarCategoria(TblCategoria categoria) throws Exception{
		TblCategoria c = findCategoriaByName(categoria.getCategoria());
		if (c == null) {
			em.persist(categoria);
		}else if (!categoria.getCategoria().equals(c.getCategoria())) {
			em.persist(categoria);
		}else {
			JSFUtil.crearMensajeWARN("Ya existe la categoria");
		}
		
		
	}
	
	public List<TblCategoria> findAllCategorias(){
		TypedQuery<TblCategoria> q = em.createQuery("SELECT c FROM TblCategoria c", TblCategoria.class);
		return q.getResultList();
	}
	
	public void eliminarCategoria(int id) {
		System.out.println("entrando al metodo eliminarCategoria");
		TblCategoria categoria = findCategoriaById(id);
		System.out.println(categoria.getCategoria());
		em.remove(categoria);
	}
	
	public TblCategoria findCategoriaById(int id) {
		return em.find(TblCategoria.class, id);
	}
	public TblCategoria findCategoriaByName(String name) {
		
		
		String consulta = "select o from TblCategoria o where o.categoria = :categoria";
		Query q = em.createQuery(consulta, TblCategoria.class).setParameter("categoria", name);
		q.setMaxResults(1);

		List<TblCategoria> categoria = q.getResultList();
		if (categoria == null || categoria.isEmpty()) {
			//devueleve un objeto vacio.
			return null;
		}
		return categoria.get(0);
	}

	public void actualizarCategoriaById(TblCategoria categoria) {
		try {

			TblCategoria c = findCategoriaById(categoria.getIdCategoria());
			System.out.println("entrando al metodo actualizar categoria");
			c.setCategoria(categoria.getCategoria());
			c.setTblCategoria(categoria.getTblCategoria());

			System.out.println(c.getCategoria());
			System.out.println(c.getTblCategoria());

			em.merge(c);
			System.out.println("actualizado");
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//consulta de categorias y subcategorias
	
	/*
	 * public List<TblCategoria> findCategoriaPrincipal() { List<TblCategoria>
	 * tblCategoria = new A; String clausula = "tbl_id_categoria is null";
	 * 
	 * try { System.out.println("entrando a la al metodo findCategoriaPrincipal");
	 * tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class,
	 * clausula, null); System.out.println(tblCategoria);
	 * 
	 * } catch (Exception e) {
	 * 
	 * } return tblCategoria; }
	 */
	
	
	public List<TblCategoria> findCategoriaPrincipal(){
		
	
		
		/*
		 * List<TblCategoria> tblCategoria = new ArrayList<TblCategoria>(); String
		 * clausula = "tbl_id_categoria is null";
		 * 
		 * try {
		 * 
		 * tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class,
		 * clausula, null); System.out.println(tblCategoria);
		 * 
		 * } catch (Exception e) {
		 * 
		 * } return tblCategoria;
		 */
		Query q;
		List<TblCategoria> listado;
		String sentenciaJPQL;
		
		sentenciaJPQL = "SELECT o FROM TblCategoria o WHERE tbl_id_categoria is null";
	
					
		q = em.createQuery(sentenciaJPQL);
		listado = q.getResultList();
		return listado;

		
		
	}

	public List<TblCategoria> findSubCategorias(int idCategoriaPrincipal) {
		/*
		 * List<TblCategoria> tblCategoria = null; String clausula =
		 * "tbl_id_categoria ='" + idCategoriaPrincipal + "'";
		 * 
		 * try {
		 * 
		 * tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class,
		 * clausula, null); System.out.println(tblCategoria);
		 * 
		 * } catch (Exception e) {
		 * 
		 * } return tblCategoria;
		 */
		
		Query q;
		List<TblCategoria> listado;
		String sentenciaJPQL;
		
		sentenciaJPQL = "SELECT o FROM TblCategoria o WHERE tbl_id_categoria = :idCategoriaPrincipal";
		
					
		q = em.createQuery(sentenciaJPQL).setParameter("idCategoriaPrincipal", idCategoriaPrincipal);
		listado = q.getResultList();
		return listado;
	}
	
	public List<TblCategoria> separarListaCategoriasSecundarias(List<TblCategoria> lista) {
		if (!lista.isEmpty()) {
			Query q;
			List<TblCategoria> listado;
			String sentenciaJPQL;
			
			sentenciaJPQL = "SELECT o FROM TblCategoria o WHERE tbl_id_categoria != null";
		
						
			q = em.createQuery(sentenciaJPQL);
			listado = q.getResultList();
			return listado;
		}
		return lista;	
		
		
		
	}
	
	/*
	 * Seccion de productos
	 * */
	
	public List<TblProducto> findAllProductos() {
		System.out.println("entrando al metodo del manager findAllProductos");
		TypedQuery<TblProducto> q = em.createQuery("SELECT p FROM TblProducto p", TblProducto.class);
		return q.getResultList();
	}
	
	public TblProducto findProductoById(int id) {
		return em.find(TblProducto.class, id);
	}

	public List<Double> calcularPreciosProducto(Double precio, double porcentaje1, double porcentaje2,double porcentaje3){
		List<Double> listaPrecios = new ArrayList<Double>();
		
		Double precio1 = precio*(porcentaje1/100)+ precio;
		Double precio2 = precio*(porcentaje2/100)+ precio;
		Double precio3 = precio*(porcentaje3/100)+ precio;
		
		listaPrecios.add(precio1);
		listaPrecios.add(precio2);
		listaPrecios.add(precio3);
		
		return listaPrecios;
		
	}
	public double calcularPorcentajeAplicado(Double precioBase, Double precioConPorcentaje) {
		Double porcentaje = (precioConPorcentaje*100)/precioBase;
		return porcentaje; 
		
	}
	public boolean verificarExistencia(List<TblDetallesCompra> listaDetalle, TblProducto producto) {
		
		boolean existe = false;
		for (int i = 0; i <listaDetalle.size(); i++) {
			
			if (producto.getDescripcion().equals(listaDetalle.get(i).getTblProducto().getDescripcion())) {
				existe= true;
			}else {
				existe= false;
			}
		}
		return existe;

	}
	
	public Double subtotalListaDetallesCompras(List<TblDetallesCompra> listaDetalle) {
		
		Double subtotal = 0.0 ;
		
		for (int i = 0; i <listaDetalle.size(); i++) {
			
			subtotal = (Integer.valueOf(listaDetalle.get(i).getCantidad())) *(listaDetalle.get(i).getPrecioCosto().doubleValue()) + subtotal;
				
		}
		return subtotal;
	}
	
	public List<TblDetallesCompra> actualizarDetalleCompraByID(List<TblDetallesCompra> listaDetalle, int id,int cantidad, double costo){
		
		for (int i = 0; i < listaDetalle.size(); i++) {
			if (listaDetalle.get(i).getIdDetallesCompras() == id) {
				listaDetalle.get(i).setPrecioCosto(BigDecimal.valueOf(costo));
				listaDetalle.get(i).setCantidad(cantidad);
				
				break;
				
			}
		}
		
		return listaDetalle;
	}
	
	public void guardarCompra(List<TblDetallesCompra> listaDetalle, TblProveedores proveedor) {
		TblCompra compra = new TblCompra();
		
		TblProveedores proveedorAux = new TblProveedores();
		proveedorAux = proveedor;
		
		compra.setTblProveedore(proveedorAux);
		Date fecha = new Date();
		compra.setFecha(fecha);
		double subtotal = subtotalListaDetallesCompras(listaDetalle) ;
		compra.setSubtotal(BigDecimal.valueOf(subtotal));
		compra.setIva(BigDecimal.valueOf(subtotal*0.12));
		compra.setTotal(BigDecimal.valueOf(subtotal+(subtotal*0.12)));
//		compra.setIva(subtotal*0.12);
		compra.setEstado(true);
		
		List<TblDetallesCompra> list = new ArrayList<TblDetallesCompra>();
		list = listaDetalle;
		compra.setTblDetallesCompras(list);
//		for (TblDetallesCompra detalle: listaDetalle) {
//			detalle.setTblCompra(compra);
//			System.out.println("viendo esto");
//			//controlStock(detalle.getTblProducto());
//		}
//	
		for (int i = 0; i < listaDetalle.size(); i++) {
			System.out.println("viendo esto: "+list.get(i).getTblProducto().getDescripcion());
			list.get(i).setTblCompra(compra);
			//em.persist(list.get(i));
			
		}
		
		em.persist(compra);
		for (int i = 0; i < listaDetalle.size(); i++) {
			System.out.println("viendo esto");
			System.out.println("mirando el stock a aumentarse: "+ list.get(i).getTblProducto().getStock());
			
			int stockNuevo  = controlStock(list.get(i));
			
			list.get(i).getTblProducto().setStock(stockNuevo);
			
			em.merge(list.get(i));
			
			
		}
		
	}
	
	public int controlStock(TblDetallesCompra detalleCompra) {
		
//		try {
			TblProducto productoAux = em.find(TblProducto.class,detalleCompra.getTblProducto().getIdProducto());
			int stockActualizado = productoAux.getStock()+detalleCompra.getCantidad();
			productoAux.setStock(stockActualizado);
			System.out.println("verificando si se actualiza el stock, stock nuevo "+ productoAux.getStock()+ " el anterior stock"+ detalleCompra.getTblProducto().getStock());
	
			return  stockActualizado;
			//em.merge(productoAux);
			
//		} catch (Exception e) {
//			System.out.println("no se pudo actualizar stock");
//			System.out.println(e);
//		}
		
	}
	public TblDetallesCompra buscarDetalleCompraTemporal(List<TblDetallesCompra> listaDetalleTemporal, TblDetallesCompra detalleDeseado) {
		
		TblDetallesCompra aux = new TblDetallesCompra();
		for (TblDetallesCompra dt : listaDetalleTemporal) {
			if (dt.getIdDetallesCompras() == detalleDeseado.getIdDetallesCompras() ) {
				aux =  dt;
				break;
			}else {
				aux= null;
			}
		}
		
		return aux;
	}
	public void actualizarDetalleTemporal(List<TblDetallesCompra> listaDetalleTemporal,TblDetallesCompra detalleDeseado) {
		if (!listaDetalleTemporal.isEmpty()) {
			for (TblDetallesCompra td : listaDetalleTemporal) {
				if (td.getIdDetallesCompras() == detalleDeseado.getIdDetallesCompras()) {
					td.setCantidad(detalleDeseado.getCantidad());
					td.setPrecioCosto(detalleDeseado.getPrecioCosto());
					td.setSubtotal( detalleDeseado.getPrecioCosto().multiply( BigDecimal.valueOf(detalleDeseado.getCantidad())));
					break;
				}
			}
		}
		
	}
	public void borrarDetalleListaTemporal(List<TblDetallesCompra> listaDetalleTemporal,TblDetallesCompra detalleDeseado) {
		if (!listaDetalleTemporal.isEmpty()) {
			for (TblDetallesCompra td : listaDetalleTemporal) {
				if (td.getIdDetallesCompras() == detalleDeseado.getIdDetallesCompras()) {
					System.out.println("Metodo del entiti manager para borrar el detalle de la lista temporal");
					listaDetalleTemporal.remove(td);
					break;
				}
			}
			//para reinicializar el id de los detalles despues de eliminar un elemento
			for (int i = 0; i < listaDetalleTemporal.size(); i++) {
				System.out.println("tama�o de la lista : "+listaDetalleTemporal.size());
				listaDetalleTemporal.get(i).setIdDetallesCompras(i+1);
			}
		}
	}
	
	public List<TblCompra> findAllCompras () {
		System.out.println("entrando al metodo del manager findAllCompras");
		TypedQuery<TblCompra> q = em.createQuery("SELECT c FROM TblCompra c ORDER BY c.idCompra desc", TblCompra.class);
		return q.getResultList();
	}
	
	public List<TblDetallesCompra> ObtenerDetalleCompraByIdCompra(int idCompra) {



		String consulta = "select d from TblDetallesCompra d JOIN d.tblCompra c where c.idCompra = :idCompra";
		Query q = em.createQuery(consulta, TblDetallesCompra.class).setParameter("idCompra", idCompra);
		//q.setMaxResults(1);

		List<TblDetallesCompra> detallesCompras = q.getResultList();
		// System.out.println("probando el otro metodo");
		if (detallesCompras == null || detallesCompras.isEmpty()) {
			return null;
		}
		// System.out.println("Haber " + proveedor.get(0).getNombreComercial());
		return detallesCompras;

	}

}
