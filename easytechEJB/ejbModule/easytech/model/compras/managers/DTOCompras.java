package easytech.model.compras.managers;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import minimarketdemo.model.core.entities.TblCompra;
import minimarketdemo.model.core.entities.TblProveedores;

public class DTOCompras implements Serializable {
	 
	private TblProveedores proveedor;
	private Date fecha;
	private BigDecimal iva;
	private BigDecimal subtotal;
	private BigDecimal total;
	private boolean estado;
	public TblProveedores getProveedor() {
		return proveedor;
	}
	public void setProveedor(TblProveedores proveedor) {
		this.proveedor = proveedor;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public BigDecimal getIva() {
		return iva;
	}
	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	

}
