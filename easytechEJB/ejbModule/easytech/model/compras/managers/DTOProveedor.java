package easytech.model.compras.managers;

import java.io.Serializable;

import minimarketdemo.model.core.entities.TblPersona;

public class DTOProveedor implements Serializable {
	private TblPersona persona;
	private boolean estado;
	private String nombreComercial;
	public TblPersona getPersona() {
		return persona;
	}
	public void setPersona(TblPersona persona) {
		this.persona = persona;
	}
	public boolean isEstado() {
		return estado;
	}
	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	public String getNombreComercial() {
		return nombreComercial;
	}
	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}
	
	
}
