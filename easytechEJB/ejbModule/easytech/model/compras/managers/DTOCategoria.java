package easytech.model.compras.managers;

import java.io.Serializable;

public class DTOCategoria implements Serializable {
	
	private int tbl_id_categoria;
	private String categoriaNombre;
	public int getTbl_id_categoria() {
		return tbl_id_categoria;
	}
	public void setTbl_id_categoria(int tbl_id_categoria) {
		this.tbl_id_categoria = tbl_id_categoria;
	}
	public String getCategoriaNombre() {
		return categoriaNombre;
	}
	public void setCategoriaNombre(String categoriaNombre) {
		this.categoriaNombre = categoriaNombre;
	}
	
	

}
