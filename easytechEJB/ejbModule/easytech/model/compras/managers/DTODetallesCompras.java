package easytech.model.compras.managers;

import java.io.Serializable;
import java.math.BigDecimal;

import minimarketdemo.model.core.entities.TblCompra;
import minimarketdemo.model.core.entities.TblProducto;

public class DTODetallesCompras implements Serializable {
	
	private TblProducto producto;
	private TblCompra compra;
	private BigDecimal precioVenta;
	private BigDecimal precioCosto;
	private int cantidad;
	private BigDecimal subtotal;
	public TblProducto getProducto() {
		return producto;
	}
	public void setProducto(TblProducto producto) {
		this.producto = producto;
	}
	public TblCompra getCompra() {
		return compra;
	}
	public void setCompra(TblCompra compra) {
		this.compra = compra;
	}
	public BigDecimal getPrecioVenta() {
		return precioVenta;
	}
	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}
	public BigDecimal getPrecioCosto() {
		return precioCosto;
	}
	public void setPrecioCosto(BigDecimal precioCosto) {
		this.precioCosto = precioCosto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public BigDecimal getSubtotal() {
		return subtotal;
	}
	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}
	
	
	
	

}
