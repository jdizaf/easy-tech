package minimarketdemo.model.ventas.managers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import minimarketdemo.model.core.entities.SegUsuario;
import minimarketdemo.model.core.entities.TblAcuerdo;
import minimarketdemo.model.core.entities.TblCategoria;
import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblCuota;
import minimarketdemo.model.core.entities.TblDetalleFactura;
import minimarketdemo.model.core.entities.TblFactPro;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.entities.TblProducto;
import minimarketdemo.model.core.entities.TblTDocumento;
import minimarketdemo.model.core.entities.TblTasaNominal;
import minimarketdemo.model.core.entities.TblTipoPago;
import minimarketdemo.model.core.managers.ManagerDAO;
import minimarketdemo.model.ventas.dtos.DTOclientes;
import minimarketdemo.model.ventas.dtos.DTOpersona;
import minimarketdemo.model.ventas.dtos.DTOproducto;

/**
 * Session Bean implementation class ManagerRenta
 */
@Stateless
@LocalBean
public class ManagerVentas {

	@PersistenceContext
	private EntityManager em;
	@EJB
	private ManagerDAO mDAO;
//Variables
	TblProducto tblProducto;
	DTOproducto dtoProducto;

	public ManagerVentas() {
		// TODO Auto-generated constructor stub
	}

	// Metodos de Ventas

	public List<TblCategoria> findBuscarCategoriasPrincipales() {
		return em.createNamedQuery("TblCategoria.findAll", TblCategoria.class).getResultList();
	}

	// Agregar Tasa Nominal
	public void insertarRegistro(TblTasaNominal tasaNominal) throws Exception {
		mDAO.insertar(tasaNominal);
	}

	public void registrarCliente(DTOpersona dtoPersona, DTOclientes dtoClientes) {
		// insertar persona
		TblPersona tblPersona = new TblPersona();
		tblPersona.setCiRuc(dtoPersona.getCedula());
		tblPersona.setNombres(dtoPersona.getNombres());
		tblPersona.setApellidos(dtoPersona.getApellidos());
		tblPersona.setTelefono(dtoPersona.getTelefono());
		tblPersona.setEmail(dtoPersona.getEmail());
		tblPersona.setDireccion(dtoPersona.getDireccion());
		em.persist(tblPersona);
		// Insertar Cliente
		TblCliente tblCliente;
		tblCliente = new TblCliente();
		tblCliente.setTblPersona(tblPersona);
		tblCliente.setEstado(dtoClientes.getEstado());
		em.persist(tblCliente);
	}

	public List<TblCliente> findAllCliente() {
		return mDAO.findWhere(TblCliente.class, "o.estado=" + "true", null);
	}

	// Consultar Facturas
	public List<TblFactPro> findAllFacturas() {
		TypedQuery<TblFactPro> q = em.createQuery("SELECT c FROM TblFactPro c WHERE id_tipo_documentos is  1 ORDER BY c.idFactura desc", TblFactPro.class);
		return q.getResultList();

	}

	// Consultar Proformas
	public List<TblFactPro> findAllProformas() {
		TypedQuery<TblFactPro> q = em.createQuery("SELECT c FROM TblFactPro c WHERE id_tipo_documentos is  2 ORDER BY c.idFactura desc", TblFactPro.class);
		return q.getResultList();
	}

	// Consultar Tasa Nominal
	public List<TblTasaNominal> findAllTasaNominal() {
		return mDAO.findAll(TblTasaNominal.class);
	}

	public List<TblTipoPago> findAllTipoPago() {
		return mDAO.findAll(TblTipoPago.class);
	}

	public List<TblTDocumento> findAllTipoDocumento() {
		return mDAO.findAll(TblTDocumento.class);
	}

	public TblTipoPago findById(int id) {
		return em.find(TblTipoPago.class, id);
	}
	

	
	public DTOpersona findClientByCedula(String cedula) {
		DTOpersona dtoPersona = new DTOpersona();
		TblPersona tblPersona = new TblPersona();
		TblCliente tblCliente = new TblCliente();
		String clausula = "ci_ruc = '" + cedula + "'";
		String clausulaCliente;
		try {

			tblPersona = (TblPersona) mDAO.findWhereObject(TblPersona.class, clausula);
			int id = tblPersona.getIdPersona();
			clausulaCliente = "id_persona='" + id + "'";
			tblCliente = (TblCliente) mDAO.findWhereObject(TblCliente.class, clausulaCliente);
			dtoPersona.setCedula(tblPersona.getCiRuc());
			dtoPersona.setNombres(tblPersona.getNombres());
			dtoPersona.setApellidos(tblPersona.getApellidos());
			dtoPersona.setTelefono(tblPersona.getTelefono());
			dtoPersona.setDireccion(tblPersona.getDireccion());
			dtoPersona.setEmail(tblPersona.getEmail());
			System.out.println(tblPersona.getApellidos());
		} catch (Exception e) {

		}
		return dtoPersona;
	}

	public List<TblFactPro> getSiguenteNroFactura(int idTipoDocumento) {
		List<TblFactPro> listaFactPro = null;

		String clausula = " id_tipo_documentos = '" + idTipoDocumento + "'";
		listaFactPro = mDAO.findWhere(TblFactPro.class, clausula, null);

		return listaFactPro;
	}

	public List<TblCategoria> findCategoriaPrincipal() {
		List<TblCategoria> tblCategoria = null;
		String clausula = "tbl_id_categoria is null";

		try {

			tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class, clausula, null);
			System.out.println(tblCategoria);

		} catch (Exception e) {

		}
		return tblCategoria;
	}

	public List<TblCategoria> findSubCategorias(int idCategoriaPrincipal) {
		List<TblCategoria> tblCategoria = null;
		String clausula = "tbl_id_categoria ='" + idCategoriaPrincipal + "'";

		try {

			tblCategoria = (List<TblCategoria>) mDAO.findWhere(TblCategoria.class, clausula, null);
			System.out.println(tblCategoria);

		} catch (Exception e) {

		}
		return tblCategoria;
	}

	public List<TblProducto> findProductos(String nombreProducto) {
		TblCategoria tblCategoria = null;
		List<TblProducto> tblProductos = null;
		String clausula = "categoria ='" + nombreProducto + "'";

		try {

			tblCategoria = (TblCategoria) mDAO.findWhereObject(TblCategoria.class, clausula);
			String clausula2 = "id_categoria ='" + tblCategoria.getIdCategoria() + "'";
			tblProductos = (List<TblProducto>) mDAO.findWhere(TblProducto.class, clausula2, null);

			System.out.println(tblCategoria);

		} catch (Exception e) {

		}
		return tblProductos;
	}

	BigDecimal bd;

	public DTOproducto findProductoById(int idProducto) {

		tblProducto = null;
		dtoProducto = new DTOproducto();
		String clausula = "id_producto ='" + idProducto + "'";

		try {
			tblProducto = (TblProducto) mDAO.findWhereObject(TblProducto.class, clausula);
			dtoProducto.setId_producto(tblProducto.getIdProducto());
			dtoProducto.setId_producto(tblProducto.getTblCategoria().getIdCategoria());

			String descripcion = tblProducto.getDescripcion();
			String[] parts = descripcion.split(", ");
			if (parts.length == 0) {
				dtoProducto.setDescripcion(tblProducto.getDescripcion());
			} else {
				dtoProducto.setNombre(parts[0]);
				dtoProducto.setMarca(parts[1]);
				dtoProducto.setModelo(parts[2]);
				dtoProducto.setSerie(parts[3]);
			}

			System.out.println("Tamaño: " + parts.length);

			bd = tblProducto.getPrecioCompra();
			double precioCompra = bd.doubleValue();
			bd = tblProducto.getPrecioVenta1();
			double precioVenta1 = bd.doubleValue();
			bd = tblProducto.getPrecioVenta2();
			double precioVenta2 = bd.doubleValue();
			bd = tblProducto.getPrecioVenta3();
			double precioVenta3 = bd.doubleValue();
			dtoProducto.setPrecio_compra(precioCompra);
			dtoProducto.setPrecio_venta1(precioVenta1);
			dtoProducto.setPrecio_venta2(precioVenta2);
			dtoProducto.setPrecio_venta3(precioVenta3);
			dtoProducto.setUrl("");
			dtoProducto.setStock(tblProducto.getStock());
			dtoProducto.setEstado(tblProducto.getEstado());

		} catch (Exception e) {

		}
		return dtoProducto;
	}

	// Consultar persona por cedula
	public TblPersona ObtenerPersonasByCedula(String cedula) {

		String consulta = "select o from TblPersona o where o.ciRuc = :cedula";
		TypedQuery<TblPersona> q = em.createQuery(consulta, TblPersona.class).setParameter("cedula", cedula);
		q.setMaxResults(1);

		List<TblPersona> persona = q.getResultList();
		System.out.println("entrando al metod de busqueda de de persona por ceddula " + persona);
		if (persona == null || persona.isEmpty()) {
			// devueleve un objeto vacio.
			return null;
		}
		return persona.get(0);
		// Fuente: https://www.iteramos.com/pregunta/29865/jpa-getsingleresult-o-null
	}

	public List<BigDecimal> crearListaprecios(List<BigDecimal> lista, List<TblProducto> listaproductos, int idproducto)
			throws Exception {
		TblProducto producto = (TblProducto) mDAO.findById(TblProducto.class, idproducto);
//		if (lista == null) {
//			for (int i = 0; i < listaproductos.size(); i++) {
//				if (listaproductos.get(i) != producto) {
//
//				}
//
//			}
//		}
		lista.add(producto.getPrecioVenta1());
		lista.add(producto.getPrecioVenta2());
		lista.add(producto.getPrecioVenta3());
		return lista;

	}

	// Consultar tipo de pago de un cliente
	public TblTipoPago ObtenerByTipoPago(String descripcion) {

		String consulta = "select o from TblTipoPago o where o.descripcion = :descripcion";
		TypedQuery<TblTipoPago> q = em.createQuery(consulta, TblTipoPago.class).setParameter("descripcion",
				descripcion);
		q.setMaxResults(1);

		List<TblTipoPago> tipoPago = q.getResultList();
		System.out.println("entrando al metod de busqueda de tipo de pago por descripcion " + tipoPago);
		if (tipoPago == null || tipoPago.isEmpty()) {
			// devueleve un objeto vacio.
			return null;
		}
		return tipoPago.get(0);
		// Fuente: https://www.iteramos.com/pregunta/29865/jpa-getsingleresult-o-null
	}

	// Consultar tipo de Documento de un cliente
	public TblTDocumento ObtenerByTipoDocumento(String descripcion) {

		String consulta = "select o from TblTDocumento o where o.descripcion = :descripcion";
		TypedQuery<TblTDocumento> q = em.createQuery(consulta, TblTDocumento.class).setParameter("descripcion",
				descripcion);
		q.setMaxResults(1);

		List<TblTDocumento> tipoDocumento = q.getResultList();
		System.out.println("entrando al metod de busqueda de tipo de pago por descripcion " + tipoDocumento);
		if (tipoDocumento == null || tipoDocumento.isEmpty()) {
			// devueleve un objeto vacio.
			return null;
		}
		return tipoDocumento.get(0);
		// Fuente: https://www.iteramos.com/pregunta/29865/jpa-getsingleresult-o-null
	}

	public TblProducto findProductoByPro(int id) {
		return em.find(TblProducto.class, id);
	}

	public boolean verificarExistencia(List<TblDetalleFactura> listaDetalle, TblProducto producto) {
		boolean existe = false;
		for (int i = 0; i < listaDetalle.size(); i++) {

			if (producto.getDescripcion().equals(listaDetalle.get(i).getTblProducto().getDescripcion())) {
				existe = true;
			} else {
				existe = false;
			}
		}
		return existe;

	}

	// SubTotal por lista
	public BigDecimal subTotalDetalleFactura(List<TblDetalleFactura> listaDetalle) {
		BigDecimal subtotal = BigDecimal.valueOf(0.0);

		for (int i = 0; i < listaDetalle.size(); i++) {

			subtotal = (BigDecimal.valueOf(listaDetalle.get(i).getCantidad())
					.multiply(listaDetalle.get(i).getPrecioselect())).add(subtotal);

		}
		return subtotal;

	}

	// Consultar Cliente por ID
	public TblCliente ObtenerClientesByCedula(String cedula) {
    	
    	TblPersona persona = ObtenerPersonasByCedula(cedula);
		int idPersona = persona.getIdPersona();

		String consulta = "select o from TblCliente o JOIN o.tblPersona p where p.idPersona = :idPersona";
		TypedQuery<TblCliente> q = em.createQuery(consulta, TblCliente.class).setParameter("idPersona", idPersona);
		q.setMaxResults(1);

		List<TblCliente> cliente = q.getResultList();
		System.out.println("entrando al metod de busqueda de de persona por ceddula " + cliente);
		if (cliente == null || cliente.isEmpty()) {
			// devueleve un objeto vacio.
			return null;
		}
		return cliente.get(0);
	}

//	public List<TblCliente> findAllClientes(int id) {
//		return mDAO.findWhere(TblCliente.class, "o.idCliente=" + "'" + personas.getIdPersona()+ "'", null);
//	}

	// Guardar Venta
	public void guardarFactura(List<TblDetalleFactura> listaDetalle, TblCliente cliente, int idtipopago,
			int idtipodocumento, int idproducto, int numFcatura, int idSegUsuario) throws Exception {

		TblCliente clienteAux = new TblCliente();
		clienteAux = cliente;

		TblTipoPago tipo = (TblTipoPago) mDAO.findById(TblTipoPago.class, idtipopago);
		TblTDocumento tipoD = (TblTDocumento) mDAO.findById(TblTDocumento.class, idtipodocumento);
		SegUsuario gerente = (SegUsuario) mDAO.findById(SegUsuario.class, idSegUsuario);
		TblFactPro cabecera = new TblFactPro();

		System.out.println("El tipo de documento es Factura");
		cabecera.setTblCliente(clienteAux);
		cabecera.setTblTipoPago(tipo);
		cabecera.setTblTDocumento(tipoD);
		cabecera.setSegUsuario(gerente);
		cabecera.setNumFaPro(numFcatura);
		cabecera.setFecha(new Date());
		BigDecimal subTotal = subTotalDetalleFactura(listaDetalle);
		cabecera.setSubtotal(subTotal);
		cabecera.setIva(subTotal.multiply(BigDecimal.valueOf(0.12)));
		cabecera.setTotal(subTotal.add(subTotal.multiply(BigDecimal.valueOf(0.12))));
		cabecera.setEstado(true);
		// Controlar Stock
		List<TblDetalleFactura> list = new ArrayList<TblDetalleFactura>();
		list = listaDetalle;
		cabecera.setTblDetalleFacturas(list);
		for (int i = 0; i < listaDetalle.size(); i++) {
			list.get(i).setTblFactPro(cabecera);
		}

		em.persist(cabecera);
		for (int i = 0; i < listaDetalle.size(); i++) {
			System.out.println("viendo esto");
			System.out.println("mirando el stock a aumentarse: "+ list.get(i).getTblProducto().getStock());
			int stockNuevo  = controlStock(list.get(i));
			
			list.get(i).getTblProducto().setStock(stockNuevo);

			em.merge(list.get(i));
		}

	}
	// Guardar Proforma
		public void guardarProforma(List<TblDetalleFactura> listaDetalle, TblCliente cliente, int idtipopago,
				int idtipodocumento, int idproducto, int numFcatura, int idSegUsuario) throws Exception {

			TblCliente clienteAux = new TblCliente();
			clienteAux = cliente;

			TblTipoPago tipo = (TblTipoPago) mDAO.findById(TblTipoPago.class, idtipopago);
			TblTDocumento tipoD = (TblTDocumento) mDAO.findById(TblTDocumento.class, idtipodocumento);
			SegUsuario gerente = (SegUsuario) mDAO.findById(SegUsuario.class, idSegUsuario);
			TblFactPro cabecera = new TblFactPro();

			System.out.println("El tipo de documento es Proforma");
			cabecera.setTblCliente(clienteAux);
			cabecera.setTblTipoPago(tipo);
			cabecera.setTblTDocumento(tipoD);
			cabecera.setSegUsuario(gerente);
			cabecera.setNumFaPro(numFcatura);
			cabecera.setFecha(new Date());
			BigDecimal subTotal = subTotalDetalleFactura(listaDetalle);
			cabecera.setSubtotal(subTotal);
			cabecera.setIva(subTotal.multiply(BigDecimal.valueOf(0.12)));
			cabecera.setTotal(subTotal.add(subTotal.multiply(BigDecimal.valueOf(0.12))));
			cabecera.setEstado(true);
			// Controlar Stock
			List<TblDetalleFactura> list = new ArrayList<TblDetalleFactura>();
			list = listaDetalle;
			cabecera.setTblDetalleFacturas(list);
			for (int i = 0; i < listaDetalle.size(); i++) {
				list.get(i).setTblFactPro(cabecera);
			}

			em.persist(cabecera);
			for (int i = 0; i < listaDetalle.size(); i++) {
				System.out.println("viendo esto");
				em.merge(list.get(i));
			}

		}
		
		//Controlar Stock
		public int controlStock(TblDetalleFactura detalleFactura) {
			TblProducto productoAux = em.find(TblProducto.class,detalleFactura.getTblProducto().getIdProducto());
			int stockActualizado = productoAux.getStock()-detalleFactura.getCantidad();
			productoAux.setStock(stockActualizado);
			System.out.println("verificando si se actualiza el stock, stock nuevo "+ productoAux.getStock()+ " el anterior stock"+ detalleFactura.getTblProducto().getStock());
	
			return  stockActualizado;

			
		}

	// Eliminar Detalle
	public void borrarDetalleListaTemporal(List<TblDetalleFactura> listaDetalleTemporal,
			TblDetalleFactura detalleDeseado) {
		if (!listaDetalleTemporal.isEmpty()) {
			for (TblDetalleFactura td : listaDetalleTemporal) {
				if (td.getIdDetalleFactura() == detalleDeseado.getIdDetalleFactura()) {
					listaDetalleTemporal.remove(td);
					break;
				}
			}
			for (int i = 0; i < listaDetalleTemporal.size(); i++) {
				listaDetalleTemporal.get(i).setIdDetalleFactura(i + 1);
			}

		}
	}
	
	//Anular Factura 
	public void anularFactura(TblFactPro factura) throws Exception {
		factura.setEstado(false);
		mDAO.actualizar(factura);
	}
	
	//Anular Factura 
		public void activarFactura(TblFactPro factura) throws Exception {
			factura.setEstado(true);
			mDAO.actualizar(factura);
		}
		
		public TblTDocumento ObtenerTipoDocumento(int idTipo) {
	    	
	    	

			String consulta = "select o from TblTDocumento  o where o.idTipoDocumentos=:idTipo";
			TypedQuery<TblTDocumento> q = em.createQuery(consulta, TblTDocumento.class).setParameter("idTipo", idTipo);
			q.setMaxResults(1);

			List<TblTDocumento> documento = q.getResultList();
			if (documento == null || documento.isEmpty()) {
				// devueleve un objeto vacio.
				return null;
			}
			return documento.get(0);
		}
		
	public TblTipoPago ObtenerTipoPago(int idTipo) {
	    	
	    	

			String consulta = "select o from TblTipoPago  o where o.idTPago=:idTipo";
			TypedQuery<TblTipoPago> q = em.createQuery(consulta, TblTipoPago.class).setParameter("idTipo", idTipo);
			q.setMaxResults(1);

			List<TblTipoPago> tipo = q.getResultList();
			if (tipo== null || tipo.isEmpty()) {
				// devueleve un objeto vacio.
				return null;
			}
			return tipo.get(0);
		}
		
	//Pasar proforma a factura
		public void redirigirProforma(TblFactPro factura, int numFactura)  throws Exception{
			TblTDocumento documento = ObtenerTipoDocumento(1);
			TblTipoPago tipo =ObtenerTipoPago(2);
			System.out.println("El tipo de Documento es: "+documento);
			System.out.println(numFactura);
			System.out.println("Ingrese al metodo redirigir");
//			TblFactPro proforma = new TblFactPro();
//			proforma.setIdFactura(factura.getIdFactura());
//			proforma.setTblCliente(factura.getTblCliente());
			factura.setTblTipoPago(tipo);
//			proforma.setTblTDocumento(documento);
//			proforma.setSegUsuario(factura.getSegUsuario());
//			proforma.setNumFaPro(numFactura);
//			proforma.setFecha(factura.getFecha());
//			proforma.setSubtotal(factura.getSubtotal());
//			proforma.setIva(factura.getIva());
//			proforma.setTotal(factura.getTotal());
//			proforma.setEstado(true);
			factura.setTblTDocumento(documento);
			factura.setNumFaPro(numFactura);
			
			mDAO.actualizar(factura);
			System.out.println("Actualice la informacion");
			
			
			
		}
		
		

	// Generar Contrato
	public void GenerarCuotasCreditoContrato(TblCliente tblCliente, double tasa__nominal, double monto, int tiempo) {
		double base;
		double exponenteA, exponenteB;
		double tasa_efectiva;
		double interes;
		double total;
		double cuotas;
		base = (1 + (tasa__nominal / 100));
		exponenteA = (tiempo * 30);
		exponenteB = exponenteA / 360;
		tasa_efectiva = (Math.pow(base, exponenteB) - 1) * 100;
		interes = monto * (tasa_efectiva / 100);
		total = monto + interes;
		cuotas = total / tiempo;
		TblAcuerdo tblAcuerdo = new TblAcuerdo();
		tblAcuerdo.setMonto(new BigDecimal(monto));
		tblAcuerdo.setEstado(true);
		tblAcuerdo.setPeriodo(tiempo);
		tblAcuerdo.setTasaEfectiva(new BigDecimal(tasa_efectiva));
		tblAcuerdo.setTblCliente(tblCliente);
		TblTasaNominal tblTasaNominal = new TblTasaNominal();
		tblTasaNominal.setIdTasaNominal(1);
		tblAcuerdo.setTblTasaNominal(tblTasaNominal);
		tblAcuerdo.setValorPorInteres(new BigDecimal(interes));
		tblAcuerdo.setTotal(new BigDecimal(total));
		Date f = new Date();
		tblAcuerdo.setFechaPrimerPago(f);
		em.persist(tblAcuerdo);
		f.setMonth(f.getMonth() + 1);

		TblCuota tblCuota = new TblCuota();
		for (int i = 0; i < tiempo; i++) {
			tblCuota = new TblCuota();
			tblCuota.setTblAcuerdo(tblAcuerdo);
			tblCuota.setEstado(true);

			tblCuota.setCuota(new BigDecimal(cuotas));
			tblCuota.setFechaPago(f);
			f = new Date(f.getYear(), (f.getMonth() + 1), f.getDate());
			em.persist(tblCuota);
			System.out.println("Exito: " + i);
		}

	}
}
