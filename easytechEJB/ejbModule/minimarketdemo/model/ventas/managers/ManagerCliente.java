package minimarketdemo.model.ventas.managers;

   
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.ejb.EJB; 
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import minimarketdemo.model.core.entities.TblCliente;
import minimarketdemo.model.core.entities.TblPersona;
import minimarketdemo.model.core.managers.ManagerDAO;
import minimarketdemo.model.core.utils.JSFUtil;









/**
 * Session Bean implementation class ManagerRenta
 */
@Stateless
@LocalBean
public class ManagerCliente {

	@EJB
	private ManagerDAO mDAO;
    /**
     * Default constructor. 
     */
	@PersistenceContext
	private EntityManager em;

    public ManagerCliente() {
        // TODO Auto-generated constructor stub
    }
    
    //Metodos de Cliente
   
   
    
    public void insertarPersona(TblPersona personas) throws Exception {
    	System.out.println("Ingrese Al metodo de registar Clinetes");
    	TblPersona p =new TblPersona();
    	p.setCiRuc(personas.getCiRuc());
    	p.setNombres(personas.getNombres());
    	p.setApellidos(personas.getApellidos());
    	p.setDireccion(personas.getDireccion());
    	p.setTelefono(personas.getTelefono());
    	p.setEmail(personas.getEmail());
    	
    	//ingresa solo si no existe en la bdd
    	TblPersona cedulaClinete =new TblPersona();
    	cedulaClinete = ObtenerPersonasByCedula(p.getCiRuc().toString());
    	if(cedulaClinete ==null) {
    		mDAO.insertar(p);
    	}
    	TblCliente clienteAux = new TblCliente();
    	clienteAux = ObtenerClientesByCedula(p.getCiRuc());
    	TblCliente c =new TblCliente();
    	System.out.println("probando si encontro al cliente"+ clienteAux);
    	if (Objects.isNull(clienteAux)) {
    		
        	c.setTblPersona(p);
        	c.setEstado(true);
        	mDAO.insertar(c);
        	JSFUtil.crearMensajeINFO("Cliente registrado");
			
		}else {
			JSFUtil.crearMensajeINFO("Cliente ya existe .");
		}
    
    }
    
    //Consultar Cedula de persona
    public TblPersona ObtenerPersonasByCedula(String cedula) {

		String consulta = "select o from TblPersona o where o.ciRuc = :cedula";
		TypedQuery<TblPersona> q = em.createQuery(consulta, TblPersona.class).setParameter("cedula", cedula);
		q.setMaxResults(1);

		List<TblPersona> persona = q.getResultList();
		System.out.println("entrando al metod de busqueda de de persona por ceddula " + persona);
		if (persona == null || persona.isEmpty()) {
			// devueleve un objeto vacio.
			return null;
		}
		return persona.get(0);
		// Fuente: https://www.iteramos.com/pregunta/29865/jpa-getsingleresult-o-null
	}
    
  //Consultar Cedula de persona
    public TblCliente ObtenerClientesByCedula(String cedula) {
    	
    	TblPersona persona = ObtenerPersonasByCedula(cedula);
		int idPersona = persona.getIdPersona();

		String consulta = "select o from TblCliente o JOIN o.tblPersona p where p.idPersona = :idPersona";
		TypedQuery<TblCliente> q = em.createQuery(consulta, TblCliente.class).setParameter("idPersona", idPersona);
		q.setMaxResults(1);

		List<TblCliente> cliente = q.getResultList();
		System.out.println("entrando al metod de busqueda de de persona por ceddula " + cliente);
		if (cliente == null || cliente.isEmpty()) {
			// devueleve un objeto vacio.
			return null;
		}
		return cliente.get(0);
		// Fuente: https://www.iteramos.com/pregunta/29865/jpa-getsingleresult-o-null
	}
    
    
    public List<TblPersona> findAllPersona(){
    	return mDAO.findAll(TblPersona.class);
    }
    
    public List<TblCliente> findAllCliente(){
    	return mDAO.findWhere(TblCliente.class, "o.estado="+"true",null);
    }
    
    public List<TblCliente> findAllClienteAll(){
    	return mDAO.findAll(TblCliente.class);
    }
    
    /*public List<TblCliente> findById(int id){
    	return mDAO.findWhere(TblCliente.class, "o.idCliente="+"'"+id+"'", null);
    }*/
    
    public TblCliente findById(int id) {
		return em.find(TblCliente.class, id);
	}
    
    public void updateClientelogico(TblCliente clientes) throws Exception {
    	clientes.setEstado(true);
    	mDAO.actualizar(clientes);
    }


    
    public void updateRegistroCliente(TblCliente clientes) {
    	try {
    		TblCliente c = findById(clientes.getIdCliente());

        	c.setEstado(clientes.getEstado());
        	c.setTblPersona(clientes.getTblPersona());
        	System.out.println(c.getTblPersona().getCiRuc());
    		  System.out.println(c.getTblPersona().getDireccion());
    		  System.out.println(c.getTblPersona().getNombres());
    		  System.out.println(c.getTblPersona().getApellidos());
    		  System.out.println(c.getTblPersona().getEmail());

        	mDAO.actualizar(c);
		} catch (Exception e) {
			// TODO: handle exception
		}
    	
    	
    }
    
    public void deleteRegistroClientelogico(TblCliente clientes) throws Exception {
    	clientes.setEstado(false);
    	mDAO.actualizar(clientes);
    }
    
	
    
}
