package minimarketdemo.model.ventas.dtos;

public class DTOproducto {
	private int id_producto;
	private int categoria;
	private String descripcion;
	private int num_ser_pro;
	private double precio_compra;
	private double precio_venta1;
	private double precio_venta2;
	private double precio_venta3;
	private String url;
	private int stock;
	private boolean estado;

	private String nombre;
	private String marca;
	private String modelo;
	private String serie;

	public DTOproducto(int id_producto, int categoria, String descripcion, int num_ser_pro, double precio_compra,
			double precio_venta1, double precio_venta2, double precio_venta3, String url, int stock, boolean estado) {
		super();
		this.id_producto = id_producto;
		this.categoria = categoria;
		this.descripcion = descripcion;
		this.num_ser_pro = num_ser_pro;
		this.precio_compra = precio_compra;
		this.precio_venta1 = precio_venta1;
		this.precio_venta2 = precio_venta2;
		this.precio_venta3 = precio_venta3;
		this.url = url;
		this.stock = stock;
		this.estado = estado;
	}

	public DTOproducto(int id_producto, int categoria, String nombre, String marca, String modelo, String serie,
			int num_ser_pro, double precio_compra, double precio_venta1, double precio_venta2, double precio_venta3,
			String url, int stock, boolean estado) {
		super();
		this.id_producto = id_producto;
		this.categoria = categoria;
		this.num_ser_pro = num_ser_pro;
		this.precio_compra = precio_compra;
		this.precio_venta1 = precio_venta1;
		this.precio_venta2 = precio_venta2;
		this.precio_venta3 = precio_venta3;
		this.url = url;
		this.stock = stock;
		this.estado = estado;
		this.nombre = nombre;
		this.marca = marca;
		this.modelo = modelo;
		this.serie = serie;
	}

	public DTOproducto() {
		super();
		this.id_producto = 0;
		this.categoria = 0;
		this.descripcion = "";
		this.num_ser_pro = 0;
		this.precio_compra = 0;
		this.precio_venta1 = 0;
		this.precio_venta2 = 0;
		this.precio_venta3 = 0;
		this.url = url;
		this.stock = 0;
		this.estado = true;
	}

	public int getId_producto() {
		return id_producto;
	}

	public void setId_producto(int id_producto) {
		this.id_producto = id_producto;
	}

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public int getNum_ser_pro() {
		return num_ser_pro;
	}

	public void setNum_ser_pro(int num_ser_pro) {
		this.num_ser_pro = num_ser_pro;
	}

	public double getPrecio_compra() {
		return precio_compra;
	}

	public void setPrecio_compra(double precio_compra) {
		this.precio_compra = precio_compra;
	}

	public double getPrecio_venta1() {
		return precio_venta1;
	}

	public void setPrecio_venta1(double precio_venta1) {
		this.precio_venta1 = precio_venta1;
	}

	public double getPrecio_venta2() {
		return precio_venta2;
	}

	public void setPrecio_venta2(double precio_venta2) {
		this.precio_venta2 = precio_venta2;
	}

	public double getPrecio_venta3() {
		return precio_venta3;
	}

	public void setPrecio_venta3(double precio_venta3) {
		this.precio_venta3 = precio_venta3;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

}
