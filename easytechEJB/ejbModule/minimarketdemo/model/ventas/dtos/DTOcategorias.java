package minimarketdemo.model.ventas.dtos;

public class DTOcategorias {
	private int idcategoria;
	private int idsubcategoria;
	private String descripcion;

	public DTOcategorias(int idcategoria, int idsubcategoria, String descripcion) {
		super();
		this.idcategoria = idcategoria;
		this.idsubcategoria = idsubcategoria;
		this.descripcion = descripcion;
	}

	public DTOcategorias() {

	}

	public DTOcategorias(int idcategoria, String descripcion) {
		super();
		this.idcategoria = idcategoria;
		this.descripcion = descripcion;
	}

	public int getIdcategoria() {
		return idcategoria;
	}

	public void setIdcategoria(int idcategoria) {
		this.idcategoria = idcategoria;
	}

	public int getIdsubcategoria() {
		return idsubcategoria;
	}

	public void setIdsubcategoria(int idsubcategoria) {
		this.idsubcategoria = idsubcategoria;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
