package minimarketdemo.model.ventas.dtos;

public class DTOclientes {

	private boolean estado;

	public DTOclientes(boolean estado) {
		super();
		this.estado = estado;
	}

	public DTOclientes() {
		super();
		this.estado = true;
	}

	public boolean getEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
