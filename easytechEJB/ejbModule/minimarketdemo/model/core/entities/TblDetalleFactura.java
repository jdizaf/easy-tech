package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tbl_detalle_facturas database table.
 * 
 */
@Entity
@Table(name="tbl_detalle_facturas")
@NamedQuery(name="TblDetalleFactura.findAll", query="SELECT t FROM TblDetalleFactura t")
public class TblDetalleFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalle_factura", unique=true, nullable=false)
	private Integer idDetalleFactura;

	private Integer cantidad;

	@Column(name="precio_total", precision=7, scale=2)
	private BigDecimal precioTotal;

	@Column(precision=7, scale=2)
	private BigDecimal precioselect;

	//bi-directional many-to-one association to TblFactPro
	@ManyToOne
	@JoinColumn(name="id_factura")
	private TblFactPro tblFactPro;

	//bi-directional many-to-one association to TblProducto
	@ManyToOne
	@JoinColumn(name="id_producto")
	private TblProducto tblProducto;

	public TblDetalleFactura() {
	}

	public Integer getIdDetalleFactura() {
		return this.idDetalleFactura;
	}

	public void setIdDetalleFactura(Integer idDetalleFactura) {
		this.idDetalleFactura = idDetalleFactura;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecioTotal() {
		return this.precioTotal;
	}

	public void setPrecioTotal(BigDecimal precioTotal) {
		this.precioTotal = precioTotal;
	}

	public BigDecimal getPrecioselect() {
		return this.precioselect;
	}

	public void setPrecioselect(BigDecimal precioselect) {
		this.precioselect = precioselect;
	}

	public TblFactPro getTblFactPro() {
		return this.tblFactPro;
	}

	public void setTblFactPro(TblFactPro tblFactPro) {
		this.tblFactPro = tblFactPro;
	}

	public TblProducto getTblProducto() {
		return this.tblProducto;
	}

	public void setTblProducto(TblProducto tblProducto) {
		this.tblProducto = tblProducto;
	}

}