package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the tbl_detalles_compra database table.
 * 
 */
@Entity
@Table(name="tbl_detalles_compra")
@NamedQuery(name="TblDetallesCompra.findAll", query="SELECT t FROM TblDetallesCompra t")
public class TblDetallesCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_detalles_compras", unique=true, nullable=false)
	private Integer idDetallesCompras;

	private Integer cantidad;

	@Column(name="precio_costo", precision=7, scale=2)
	private BigDecimal precioCosto;

	@Column(name="precio_venta", precision=7, scale=2)
	private BigDecimal precioVenta;

	@Column(precision=7, scale=2)
	private BigDecimal subtotal;

	//bi-directional many-to-one association to TblCompra
	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name="id_compra", nullable = false, updatable = false)
	private TblCompra tblCompra;

	//bi-directional many-to-one association to TblProducto
	@ManyToOne( cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_producto")
	private TblProducto tblProducto;

	public TblDetallesCompra() {
	}

	public Integer getIdDetallesCompras() {
		return this.idDetallesCompras;
	}

	public void setIdDetallesCompras(Integer idDetallesCompras) {
		this.idDetallesCompras = idDetallesCompras;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getPrecioCosto() {
		return this.precioCosto;
	}

	public void setPrecioCosto(BigDecimal precioCosto) {
		this.precioCosto = precioCosto;
	}

	public BigDecimal getPrecioVenta() {
		return this.precioVenta;
	}

	public void setPrecioVenta(BigDecimal precioVenta) {
		this.precioVenta = precioVenta;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public TblCompra getTblCompra() {
		return this.tblCompra;
	}

	public void setTblCompra(TblCompra tblCompra) {
		this.tblCompra = tblCompra;
	}

	public TblProducto getTblProducto() {
		return this.tblProducto;
	}

	public void setTblProducto(TblProducto tblProducto) {
		this.tblProducto = tblProducto;
	}

}