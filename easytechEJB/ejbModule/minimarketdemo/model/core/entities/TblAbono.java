package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The persistent class for the tbl_abonos database table.
 * 
 */
@Entity
@Table(name="tbl_abonos")
@NamedQuery(name="TblAbono.findAll", query="SELECT t FROM TblAbono t")
public class TblAbono implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_abono", unique=true, nullable=false)
	private Integer idAbono;

	@Column(precision=7, scale=2)
	private BigDecimal abono;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_abono")
	private Date fechaAbono;

	@Column(precision=7, scale=2)
	private BigDecimal saldo;

	//bi-directional many-to-one association to TblCuota
	@ManyToOne
	@JoinColumn(name="id_cuota")
	private TblCuota tblCuota;

	public TblAbono() {
	}

	public Integer getIdAbono() {
		return this.idAbono;
	}

	public void setIdAbono(Integer idAbono) {
		this.idAbono = idAbono;
	}

	public BigDecimal getAbono() {
		return this.abono;
	}

	public void setAbono(BigDecimal abono) {
		this.abono = abono;
	}

	public Date getFechaAbono() {
		return this.fechaAbono;
	}

	public void setFechaAbono(Date fechaAbono) {
		this.fechaAbono = fechaAbono;
	}

	public BigDecimal getSaldo() {
		return this.saldo;
	}

	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public TblCuota getTblCuota() {
		return this.tblCuota;
	}

	public void setTblCuota(TblCuota tblCuota) {
		this.tblCuota = tblCuota;
	}

}