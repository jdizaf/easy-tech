package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tbl_cuotas database table.
 * 
 */
@Entity
@Table(name="tbl_cuotas")
@NamedQuery(name="TblCuota.findAll", query="SELECT t FROM TblCuota t")
public class TblCuota implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cuota", unique=true, nullable=false)
	private Integer idCuota;

	@Column(precision=7, scale=2)
	private BigDecimal cuota;

	@Column(name="dias_vencidos")
	private Integer diasVencidos;

	@Column(name="dias_x_vencer")
	private Integer diasXVencer;

	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_pago")
	private Date fechaPago;

	//bi-directional many-to-one association to TblAbono
	@OneToMany(mappedBy="tblCuota")
	private List<TblAbono> tblAbonos;

	//bi-directional many-to-one association to TblAcuerdo
	@ManyToOne
	@JoinColumn(name="id_acuerdo")
	private TblAcuerdo tblAcuerdo;

	public TblCuota() {
	}

	public Integer getIdCuota() {
		return this.idCuota;
	}

	public void setIdCuota(Integer idCuota) {
		this.idCuota = idCuota;
	}

	public BigDecimal getCuota() {
		return this.cuota;
	}

	public void setCuota(BigDecimal cuota) {
		this.cuota = cuota;
	}

	public Integer getDiasVencidos() {
		return this.diasVencidos;
	}

	public void setDiasVencidos(Integer diasVencidos) {
		this.diasVencidos = diasVencidos;
	}

	public Integer getDiasXVencer() {
		return this.diasXVencer;
	}

	public void setDiasXVencer(Integer diasXVencer) {
		this.diasXVencer = diasXVencer;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaPago() {
		return this.fechaPago;
	}

	public void setFechaPago(Date fechaPago) {
		this.fechaPago = fechaPago;
	}

	public List<TblAbono> getTblAbonos() {
		return this.tblAbonos;
	}

	public void setTblAbonos(List<TblAbono> tblAbonos) {
		this.tblAbonos = tblAbonos;
	}

	public TblAbono addTblAbono(TblAbono tblAbono) {
		getTblAbonos().add(tblAbono);
		tblAbono.setTblCuota(this);

		return tblAbono;
	}

	public TblAbono removeTblAbono(TblAbono tblAbono) {
		getTblAbonos().remove(tblAbono);
		tblAbono.setTblCuota(null);

		return tblAbono;
	}

	public TblAcuerdo getTblAcuerdo() {
		return this.tblAcuerdo;
	}

	public void setTblAcuerdo(TblAcuerdo tblAcuerdo) {
		this.tblAcuerdo = tblAcuerdo;
	}

}