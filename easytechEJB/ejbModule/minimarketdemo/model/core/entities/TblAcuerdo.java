package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tbl_acuerdos database table.
 * 
 */
@Entity
@Table(name="tbl_acuerdos")
@NamedQuery(name="TblAcuerdo.findAll", query="SELECT t FROM TblAcuerdo t")
public class TblAcuerdo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_acuerdo", unique=true, nullable=false)
	private Integer idAcuerdo;

	private Boolean estado;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_primer_pago")
	private Date fechaPrimerPago;

	@Column(precision=7, scale=2)
	private BigDecimal monto;

	private Integer periodo;

	@Column(name="tasa_efectiva", precision=7, scale=2)
	private BigDecimal tasaEfectiva;

	@Column(precision=7, scale=2)
	private BigDecimal total;

	@Column(name="valor_por_interes", precision=7, scale=2)
	private BigDecimal valorPorInteres;

	//bi-directional many-to-one association to TblCliente
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private TblCliente tblCliente;

	//bi-directional many-to-one association to TblTasaNominal
	@ManyToOne
	@JoinColumn(name="id_tasa_nominal")
	private TblTasaNominal tblTasaNominal;

	//bi-directional many-to-one association to TblCuota
	@OneToMany(mappedBy="tblAcuerdo")
	private List<TblCuota> tblCuotas;

	public TblAcuerdo() {
	}

	public Integer getIdAcuerdo() {
		return this.idAcuerdo;
	}

	public void setIdAcuerdo(Integer idAcuerdo) {
		this.idAcuerdo = idAcuerdo;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFechaPrimerPago() {
		return this.fechaPrimerPago;
	}

	public void setFechaPrimerPago(Date fechaPrimerPago) {
		this.fechaPrimerPago = fechaPrimerPago;
	}

	public BigDecimal getMonto() {
		return this.monto;
	}

	public void setMonto(BigDecimal monto) {
		this.monto = monto;
	}

	public Integer getPeriodo() {
		return this.periodo;
	}

	public void setPeriodo(Integer periodo) {
		this.periodo = periodo;
	}

	public BigDecimal getTasaEfectiva() {
		return this.tasaEfectiva;
	}

	public void setTasaEfectiva(BigDecimal tasaEfectiva) {
		this.tasaEfectiva = tasaEfectiva;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getValorPorInteres() {
		return this.valorPorInteres;
	}

	public void setValorPorInteres(BigDecimal valorPorInteres) {
		this.valorPorInteres = valorPorInteres;
	}

	public TblCliente getTblCliente() {
		return this.tblCliente;
	}

	public void setTblCliente(TblCliente tblCliente) {
		this.tblCliente = tblCliente;
	}

	public TblTasaNominal getTblTasaNominal() {
		return this.tblTasaNominal;
	}

	public void setTblTasaNominal(TblTasaNominal tblTasaNominal) {
		this.tblTasaNominal = tblTasaNominal;
	}

	public List<TblCuota> getTblCuotas() {
		return this.tblCuotas;
	}

	public void setTblCuotas(List<TblCuota> tblCuotas) {
		this.tblCuotas = tblCuotas;
	}

	public TblCuota addTblCuota(TblCuota tblCuota) {
		getTblCuotas().add(tblCuota);
		tblCuota.setTblAcuerdo(this);

		return tblCuota;
	}

	public TblCuota removeTblCuota(TblCuota tblCuota) {
		getTblCuotas().remove(tblCuota);
		tblCuota.setTblAcuerdo(null);

		return tblCuota;
	}

}