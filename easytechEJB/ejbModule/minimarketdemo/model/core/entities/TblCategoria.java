package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_categorias database table.
 * 
 */
@Entity
@Table(name="tbl_categorias")
@NamedQuery(name="TblCategoria.findAll", query="SELECT t FROM TblCategoria t")
public class TblCategoria implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_categoria", unique=true, nullable=false)
	private Integer idCategoria;

	@Column(length=50)
	private String categoria;

	//bi-directional many-to-one association to TblCategoria
	@ManyToOne(cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	@JoinColumn(name="tbl_id_categoria")
	private TblCategoria tblCategoria;

	//bi-directional many-to-one association to TblCategoria
	@OneToMany(mappedBy="tblCategoria",cascade = CascadeType.MERGE,fetch = FetchType.EAGER)
	private List<TblCategoria> tblCategorias;

	//bi-directional many-to-one association to TblProducto
	@OneToMany(mappedBy="tblCategoria")
	private List<TblProducto> tblProductos;

	public TblCategoria() {
	}

	public Integer getIdCategoria() {
		return this.idCategoria;
	}

	public void setIdCategoria(Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getCategoria() {
		return this.categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public TblCategoria getTblCategoria() {
		return this.tblCategoria;
	}

	public void setTblCategoria(TblCategoria tblCategoria) {
		this.tblCategoria = tblCategoria;
	}

	public List<TblCategoria> getTblCategorias() {
		return this.tblCategorias;
	}

	public void setTblCategorias(List<TblCategoria> tblCategorias) {
		this.tblCategorias = tblCategorias;
	}

	public TblCategoria addTblCategoria(TblCategoria tblCategoria) {
		getTblCategorias().add(tblCategoria);
		tblCategoria.setTblCategoria(this);

		return tblCategoria;
	}

	public TblCategoria removeTblCategoria(TblCategoria tblCategoria) {
		getTblCategorias().remove(tblCategoria);
		tblCategoria.setTblCategoria(null);

		return tblCategoria;
	}

	public List<TblProducto> getTblProductos() {
		return this.tblProductos;
	}

	public void setTblProductos(List<TblProducto> tblProductos) {
		this.tblProductos = tblProductos;
	}

	public TblProducto addTblProducto(TblProducto tblProducto) {
		getTblProductos().add(tblProducto);
		tblProducto.setTblCategoria(this);

		return tblProducto;
	}

	public TblProducto removeTblProducto(TblProducto tblProducto) {
		getTblProductos().remove(tblProducto);
		tblProducto.setTblCategoria(null);

		return tblProducto;
	}

}