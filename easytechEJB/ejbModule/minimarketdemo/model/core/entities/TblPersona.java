package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_personas database table.
 * 
 */
@Entity
@Table(name="tbl_personas")
@NamedQuery(name="TblPersona.findAll", query="SELECT t FROM TblPersona t")
public class TblPersona implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_persona", unique=true, nullable=false)
	private Integer idPersona;

	@Column(length=50)
	private String apellidos;

	@Column(name="ci_ruc", length=15)
	private String ciRuc;

	@Column(length=60)
	private String direccion;

	@Column(length=50)
	private String email;

	@Column(length=50)
	private String nombres;

	@Column(length=10)
	private String telefono;

	//bi-directional many-to-one association to TblCliente
	@OneToMany(mappedBy="tblPersona")
	private List<TblCliente> tblClientes;

	//bi-directional many-to-one association to TblProveedores
	@OneToMany(mappedBy="tblPersona")
	private List<TblProveedores> tblProveedores;

	public TblPersona() {
	}

	public Integer getIdPersona() {
		return this.idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCiRuc() {
		return this.ciRuc;
	}

	public void setCiRuc(String ciRuc) {
		this.ciRuc = ciRuc;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNombres() {
		return this.nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<TblCliente> getTblClientes() {
		return this.tblClientes;
	}

	public void setTblClientes(List<TblCliente> tblClientes) {
		this.tblClientes = tblClientes;
	}

	public TblCliente addTblCliente(TblCliente tblCliente) {
		getTblClientes().add(tblCliente);
		tblCliente.setTblPersona(this);

		return tblCliente;
	}

	public TblCliente removeTblCliente(TblCliente tblCliente) {
		getTblClientes().remove(tblCliente);
		tblCliente.setTblPersona(null);

		return tblCliente;
	}

	public List<TblProveedores> getTblProveedores() {
		return this.tblProveedores;
	}

	public void setTblProveedores(List<TblProveedores> tblProveedores) {
		this.tblProveedores = tblProveedores;
	}

	public TblProveedores addTblProveedore(TblProveedores tblProveedore) {
		getTblProveedores().add(tblProveedore);
		tblProveedore.setTblPersona(this);

		return tblProveedore;
	}

	public TblProveedores removeTblProveedore(TblProveedores tblProveedore) {
		getTblProveedores().remove(tblProveedore);
		tblProveedore.setTblPersona(null);

		return tblProveedore;
	}

}