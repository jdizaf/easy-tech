package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import javax.transaction.TransactionScoped;
import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tbl_compras database table.
 * 
 */
@Entity
@Table(name="tbl_compras")
@NamedQuery(name="TblCompra.findAll", query="SELECT t FROM TblCompra t")
public class TblCompra implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_compra", unique=true, nullable=false)
	private Integer idCompra;

	private Boolean estado;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(precision=7, scale=2)
	private BigDecimal iva;

	@Column(precision=7, scale=2)
	private BigDecimal subtotal;

	@Column(precision=7, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to TblProveedores
	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name="id_proveedor")
	private TblProveedores tblProveedore;
	
	//bi-directional many-to-one association to TblDetallesCompra
	@OneToMany(mappedBy="tblCompra", cascade = CascadeType.MERGE)
	private List<TblDetallesCompra> tblDetallesCompras;

	public TblCompra() {
	}

	public Integer getIdCompra() {
		return this.idCompra;
	}

	public void setIdCompra(Integer idCompra) {
		this.idCompra = idCompra;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public TblProveedores getTblProveedore() {
		return this.tblProveedore;
	}

	public void setTblProveedore(TblProveedores tblProveedore) {
		this.tblProveedore = tblProveedore;
	}

	public List<TblDetallesCompra> getTblDetallesCompras() {
		return this.tblDetallesCompras;
	}

	public void setTblDetallesCompras(List<TblDetallesCompra> tblDetallesCompras) {
		this.tblDetallesCompras = tblDetallesCompras;
	}

	public TblDetallesCompra addTblDetallesCompra(TblDetallesCompra tblDetallesCompra) {
		getTblDetallesCompras().add(tblDetallesCompra);
		tblDetallesCompra.setTblCompra(this);

		return tblDetallesCompra;
	}

	public TblDetallesCompra removeTblDetallesCompra(TblDetallesCompra tblDetallesCompra) {
		getTblDetallesCompras().remove(tblDetallesCompra);
		tblDetallesCompra.setTblCompra(null);

		return tblDetallesCompra;
	}

}