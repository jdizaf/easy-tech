package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tbl_fact_pro database table.
 * 
 */
@Entity
@Table(name="tbl_fact_pro")
@NamedQuery(name="TblFactPro.findAll", query="SELECT t FROM TblFactPro t")
public class TblFactPro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_factura", unique=true, nullable=false)
	private Integer idFactura;

	private Boolean estado;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(precision=7, scale=2)
	private BigDecimal iva;

	@Column(name="num_fa_pro")
	private Integer numFaPro;

	@Column(precision=7, scale=2)
	private BigDecimal subtotal;

	@Column(precision=7, scale=2)
	private BigDecimal total;

	//bi-directional many-to-one association to TblDetalleFactura
	@OneToMany(mappedBy="tblFactPro")
	private List<TblDetalleFactura> tblDetalleFacturas;

	//bi-directional many-to-one association to SegUsuario
	@ManyToOne
	@JoinColumn(name="id_seg_usuario")
	private SegUsuario segUsuario;

	//bi-directional many-to-one association to TblCliente
	@ManyToOne
	@JoinColumn(name="id_cliente")
	private TblCliente tblCliente;

	//bi-directional many-to-one association to TblTDocumento
	@ManyToOne
	@JoinColumn(name="id_tipo_documentos")
	private TblTDocumento tblTDocumento;

	//bi-directional many-to-one association to TblTipoPago
	@ManyToOne
	@JoinColumn(name="id_t_pago")
	private TblTipoPago tblTipoPago;

	public TblFactPro() {
	}

	public Integer getIdFactura() {
		return this.idFactura;
	}

	public void setIdFactura(Integer idFactura) {
		this.idFactura = idFactura;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public BigDecimal getIva() {
		return this.iva;
	}

	public void setIva(BigDecimal iva) {
		this.iva = iva;
	}

	public Integer getNumFaPro() {
		return this.numFaPro;
	}

	public void setNumFaPro(Integer numFaPro) {
		this.numFaPro = numFaPro;
	}

	public BigDecimal getSubtotal() {
		return this.subtotal;
	}

	public void setSubtotal(BigDecimal subtotal) {
		this.subtotal = subtotal;
	}

	public BigDecimal getTotal() {
		return this.total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<TblDetalleFactura> getTblDetalleFacturas() {
		return this.tblDetalleFacturas;
	}

	public void setTblDetalleFacturas(List<TblDetalleFactura> tblDetalleFacturas) {
		this.tblDetalleFacturas = tblDetalleFacturas;
	}

	public TblDetalleFactura addTblDetalleFactura(TblDetalleFactura tblDetalleFactura) {
		getTblDetalleFacturas().add(tblDetalleFactura);
		tblDetalleFactura.setTblFactPro(this);

		return tblDetalleFactura;
	}

	public TblDetalleFactura removeTblDetalleFactura(TblDetalleFactura tblDetalleFactura) {
		getTblDetalleFacturas().remove(tblDetalleFactura);
		tblDetalleFactura.setTblFactPro(null);

		return tblDetalleFactura;
	}

	public SegUsuario getSegUsuario() {
		return this.segUsuario;
	}

	public void setSegUsuario(SegUsuario segUsuario) {
		this.segUsuario = segUsuario;
	}

	public TblCliente getTblCliente() {
		return this.tblCliente;
	}

	public void setTblCliente(TblCliente tblCliente) {
		this.tblCliente = tblCliente;
	}

	public TblTDocumento getTblTDocumento() {
		return this.tblTDocumento;
	}

	public void setTblTDocumento(TblTDocumento tblTDocumento) {
		this.tblTDocumento = tblTDocumento;
	}

	public TblTipoPago getTblTipoPago() {
		return this.tblTipoPago;
	}

	public void setTblTipoPago(TblTipoPago tblTipoPago) {
		this.tblTipoPago = tblTipoPago;
	}

}