package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_clientes database table.
 * 
 */
@Entity
@Table(name="tbl_clientes")
@NamedQuery(name="TblCliente.findAll", query="SELECT t FROM TblCliente t")
public class TblCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_cliente", unique=true, nullable=false)
	private Integer idCliente;

	private Boolean estado;

	//bi-directional many-to-one association to TblAcuerdo
	@OneToMany(mappedBy="tblCliente", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<TblAcuerdo> tblAcuerdos;

	//bi-directional many-to-one association to TblPersona
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(name="id_persona")
	private TblPersona tblPersona;

	//bi-directional many-to-one association to TblFactPro
	@OneToMany(mappedBy="tblCliente")
	private List<TblFactPro> tblFactPros;

	public TblCliente() {
	}

	public Integer getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public List<TblAcuerdo> getTblAcuerdos() {
		return this.tblAcuerdos;
	}

	public void setTblAcuerdos(List<TblAcuerdo> tblAcuerdos) {
		this.tblAcuerdos = tblAcuerdos;
	}

	public TblAcuerdo addTblAcuerdo(TblAcuerdo tblAcuerdo) {
		getTblAcuerdos().add(tblAcuerdo);
		tblAcuerdo.setTblCliente(this);

		return tblAcuerdo;
	}

	public TblAcuerdo removeTblAcuerdo(TblAcuerdo tblAcuerdo) {
		getTblAcuerdos().remove(tblAcuerdo);
		tblAcuerdo.setTblCliente(null);

		return tblAcuerdo;
	}

	public TblPersona getTblPersona() {
		return this.tblPersona;
	}

	public void setTblPersona(TblPersona tblPersona) {
		this.tblPersona = tblPersona;
	}

	public List<TblFactPro> getTblFactPros() {
		return this.tblFactPros;
	}

	public void setTblFactPros(List<TblFactPro> tblFactPros) {
		this.tblFactPros = tblFactPros;
	}

	public TblFactPro addTblFactPro(TblFactPro tblFactPro) {
		getTblFactPros().add(tblFactPro);
		tblFactPro.setTblCliente(this);

		return tblFactPro;
	}

	public TblFactPro removeTblFactPro(TblFactPro tblFactPro) {
		getTblFactPros().remove(tblFactPro);
		tblFactPro.setTblCliente(null);

		return tblFactPro;
	}

}