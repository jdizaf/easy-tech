package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_proveedores database table.
 * 
 */
@Entity
@Table(name="tbl_proveedores")
@NamedQuery(name="TblProveedores.findAll", query="SELECT t FROM TblProveedores t")
public class TblProveedores implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_proveedor", unique=true, nullable=false)
	private Integer idProveedor;

	private Boolean estado;

	@Column(name="nombre_comercial", length=60)
	private String nombreComercial;

	//bi-directional many-to-one association to TblCompra
	@OneToMany(mappedBy="tblProveedore",cascade = CascadeType.MERGE)
	private List<TblCompra> tblCompras;

	//bi-directional many-to-one association to TblPersona
	@ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
	@JoinColumn(name="id_persona")
	private TblPersona tblPersona;

	public TblProveedores() {
	}

	public Integer getIdProveedor() {
		return this.idProveedor;
	}

	public void setIdProveedor(Integer idProveedor) {
		this.idProveedor = idProveedor;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getNombreComercial() {
		return this.nombreComercial;
	}

	public void setNombreComercial(String nombreComercial) {
		this.nombreComercial = nombreComercial;
	}

	public List<TblCompra> getTblCompras() {
		return this.tblCompras;
	}

	public void setTblCompras(List<TblCompra> tblCompras) {
		this.tblCompras = tblCompras;
	}

	public TblCompra addTblCompra(TblCompra tblCompra) {
		getTblCompras().add(tblCompra);
		tblCompra.setTblProveedore(this);

		return tblCompra;
	}

	public TblCompra removeTblCompra(TblCompra tblCompra) {
		getTblCompras().remove(tblCompra);
		tblCompra.setTblProveedore(null);

		return tblCompra;
	}

	public TblPersona getTblPersona() {
		return this.tblPersona;
	}

	public void setTblPersona(TblPersona tblPersona) {
		this.tblPersona = tblPersona;
	}

}