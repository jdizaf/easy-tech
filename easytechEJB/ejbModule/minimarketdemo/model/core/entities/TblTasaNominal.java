package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the tbl_tasa_nominal database table.
 * 
 */
@Entity
@Table(name="tbl_tasa_nominal")
@NamedQuery(name="TblTasaNominal.findAll", query="SELECT t FROM TblTasaNominal t")
public class TblTasaNominal implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tasa_nominal", unique=true, nullable=false)
	private Integer idTasaNominal;

	@Column(precision=7, scale=2)
	private BigDecimal porcentaje;

	@Column(name="tasa_nominal", length=50)
	private String tasaNominal;

	//bi-directional many-to-one association to TblAcuerdo
	@OneToMany(mappedBy="tblTasaNominal")
	private List<TblAcuerdo> tblAcuerdos;

	public TblTasaNominal() {
	}

	public Integer getIdTasaNominal() {
		return this.idTasaNominal;
	}

	public void setIdTasaNominal(Integer idTasaNominal) {
		this.idTasaNominal = idTasaNominal;
	}

	public BigDecimal getPorcentaje() {
		return this.porcentaje;
	}

	public void setPorcentaje(BigDecimal porcentaje) {
		this.porcentaje = porcentaje;
	}

	public String getTasaNominal() {
		return this.tasaNominal;
	}

	public void setTasaNominal(String tasaNominal) {
		this.tasaNominal = tasaNominal;
	}

	public List<TblAcuerdo> getTblAcuerdos() {
		return this.tblAcuerdos;
	}

	public void setTblAcuerdos(List<TblAcuerdo> tblAcuerdos) {
		this.tblAcuerdos = tblAcuerdos;
	}

	public TblAcuerdo addTblAcuerdo(TblAcuerdo tblAcuerdo) {
		getTblAcuerdos().add(tblAcuerdo);
		tblAcuerdo.setTblTasaNominal(this);

		return tblAcuerdo;
	}

	public TblAcuerdo removeTblAcuerdo(TblAcuerdo tblAcuerdo) {
		getTblAcuerdos().remove(tblAcuerdo);
		tblAcuerdo.setTblTasaNominal(null);

		return tblAcuerdo;
	}

}