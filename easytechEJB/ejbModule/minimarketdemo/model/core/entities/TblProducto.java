package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


/**
 * The persistent class for the tbl_productos database table.
 * 
 */
@Entity
@Table(name="tbl_productos")
@NamedQuery(name="TblProducto.findAll", query="SELECT t FROM TblProducto t")
public class TblProducto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_producto", unique=true, nullable=false)
	private Integer idProducto;

	@Column(length=100)
	private String descripcion;

	private Boolean estado;

	private byte[] imagen;

	@Column(name="precio_compra", precision=7, scale=2)
	private BigDecimal precioCompra;

	@Column(name="precio_venta_1", precision=7, scale=2)
	private BigDecimal precioVenta1;

	@Column(name="precio_venta_2", precision=7, scale=2)
	private BigDecimal precioVenta2;

	@Column(name="precio_venta_3", precision=7, scale=2)
	private BigDecimal precioVenta3;

	private Integer stock;

	//bi-directional many-to-one association to TblDetalleFactura
	@OneToMany(mappedBy="tblProducto")
	private List<TblDetalleFactura> tblDetalleFacturas;

	//bi-directional many-to-one association to TblDetallesCompra
	@OneToMany(mappedBy="tblProducto")
	private List<TblDetallesCompra> tblDetallesCompras;

	//bi-directional many-to-one association to TblCategoria
	@ManyToOne
	@JoinColumn(name="id_categoria")
	private TblCategoria tblCategoria;

	public TblProducto() {
	}

	public Integer getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(Integer idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public byte[] getImagen() {
		return this.imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public BigDecimal getPrecioCompra() {
		return this.precioCompra;
	}

	public void setPrecioCompra(BigDecimal precioCompra) {
		this.precioCompra = precioCompra;
	}

	public BigDecimal getPrecioVenta1() {
		return this.precioVenta1;
	}

	public void setPrecioVenta1(BigDecimal precioVenta1) {
		this.precioVenta1 = precioVenta1;
	}

	public BigDecimal getPrecioVenta2() {
		return this.precioVenta2;
	}

	public void setPrecioVenta2(BigDecimal precioVenta2) {
		this.precioVenta2 = precioVenta2;
	}

	public BigDecimal getPrecioVenta3() {
		return this.precioVenta3;
	}

	public void setPrecioVenta3(BigDecimal precioVenta3) {
		this.precioVenta3 = precioVenta3;
	}

	public Integer getStock() {
		return this.stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public List<TblDetalleFactura> getTblDetalleFacturas() {
		return this.tblDetalleFacturas;
	}

	public void setTblDetalleFacturas(List<TblDetalleFactura> tblDetalleFacturas) {
		this.tblDetalleFacturas = tblDetalleFacturas;
	}

	public TblDetalleFactura addTblDetalleFactura(TblDetalleFactura tblDetalleFactura) {
		getTblDetalleFacturas().add(tblDetalleFactura);
		tblDetalleFactura.setTblProducto(this);

		return tblDetalleFactura;
	}

	public TblDetalleFactura removeTblDetalleFactura(TblDetalleFactura tblDetalleFactura) {
		getTblDetalleFacturas().remove(tblDetalleFactura);
		tblDetalleFactura.setTblProducto(null);

		return tblDetalleFactura;
	}

	public List<TblDetallesCompra> getTblDetallesCompras() {
		return this.tblDetallesCompras;
	}

	public void setTblDetallesCompras(List<TblDetallesCompra> tblDetallesCompras) {
		this.tblDetallesCompras = tblDetallesCompras;
	}

	public TblDetallesCompra addTblDetallesCompra(TblDetallesCompra tblDetallesCompra) {
		getTblDetallesCompras().add(tblDetallesCompra);
		tblDetallesCompra.setTblProducto(this);

		return tblDetallesCompra;
	}

	public TblDetallesCompra removeTblDetallesCompra(TblDetallesCompra tblDetallesCompra) {
		getTblDetallesCompras().remove(tblDetallesCompra);
		tblDetallesCompra.setTblProducto(null);

		return tblDetallesCompra;
	}

	public TblCategoria getTblCategoria() {
		return this.tblCategoria;
	}

	public void setTblCategoria(TblCategoria tblCategoria) {
		this.tblCategoria = tblCategoria;
	}

}