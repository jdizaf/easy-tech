package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_tipo_pagos database table.
 * 
 */
@Entity
@Table(name="tbl_tipo_pagos")
@NamedQuery(name="TblTipoPago.findAll", query="SELECT t FROM TblTipoPago t")
public class TblTipoPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_t_pago", unique=true, nullable=false)
	private Integer idTPago;

	@Column(length=100)
	private String descripcion;

	//bi-directional many-to-one association to TblFactPro
	@OneToMany(mappedBy="tblTipoPago")
	private List<TblFactPro> tblFactPros;

	public TblTipoPago() {
	}

	public Integer getIdTPago() {
		return this.idTPago;
	}

	public void setIdTPago(Integer idTPago) {
		this.idTPago = idTPago;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<TblFactPro> getTblFactPros() {
		return this.tblFactPros;
	}

	public void setTblFactPros(List<TblFactPro> tblFactPros) {
		this.tblFactPros = tblFactPros;
	}

	public TblFactPro addTblFactPro(TblFactPro tblFactPro) {
		getTblFactPros().add(tblFactPro);
		tblFactPro.setTblTipoPago(this);

		return tblFactPro;
	}

	public TblFactPro removeTblFactPro(TblFactPro tblFactPro) {
		getTblFactPros().remove(tblFactPro);
		tblFactPro.setTblTipoPago(null);

		return tblFactPro;
	}

}