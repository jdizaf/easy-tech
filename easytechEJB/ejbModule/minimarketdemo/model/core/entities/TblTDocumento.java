package minimarketdemo.model.core.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_t_documentos database table.
 * 
 */
@Entity
@Table(name="tbl_t_documentos")
@NamedQuery(name="TblTDocumento.findAll", query="SELECT t FROM TblTDocumento t")
public class TblTDocumento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_documentos", unique=true, nullable=false)
	private Integer idTipoDocumentos;

	@Column(length=100)
	private String descripcion;

	//bi-directional many-to-one association to TblFactPro
	@OneToMany(mappedBy="tblTDocumento")
	private List<TblFactPro> tblFactPros;

	public TblTDocumento() {
	}

	public Integer getIdTipoDocumentos() {
		return this.idTipoDocumentos;
	}

	public void setIdTipoDocumentos(Integer idTipoDocumentos) {
		this.idTipoDocumentos = idTipoDocumentos;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<TblFactPro> getTblFactPros() {
		return this.tblFactPros;
	}

	public void setTblFactPros(List<TblFactPro> tblFactPros) {
		this.tblFactPros = tblFactPros;
	}

	public TblFactPro addTblFactPro(TblFactPro tblFactPro) {
		getTblFactPros().add(tblFactPro);
		tblFactPro.setTblTDocumento(this);

		return tblFactPro;
	}

	public TblFactPro removeTblFactPro(TblFactPro tblFactPro) {
		getTblFactPros().remove(tblFactPro);
		tblFactPro.setTblTDocumento(null);

		return tblFactPro;
	}

}